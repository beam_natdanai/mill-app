import  React,{ useState, useEffect } from 'react';
import { Layout, Menu, Avatar, Image, Drawer } from "antd";
import { useRouter } from 'next/router';
import { signOut, getUser } from '../../lib/user.js';
import { isMobile } from 'react-device-detect';
import { useAppContext } from '../../context/state';
import Link from 'next/link'
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    LogoutOutlined,
    BarChartOutlined,
    ArrowRightOutlined,
    ArrowLeftOutlined,
    PieChartOutlined,
    SettingOutlined,
    DatabaseOutlined,
    TeamOutlined,
    FontSizeOutlined,
    BookOutlined,
    FileImageOutlined,
    SoundOutlined,
    EditOutlined,
    AreaChartOutlined
} from "@ant-design/icons";
const { SubMenu } = Menu;
const { Header, Sider, Content } = Layout;

const MainMenu = (props) => {

    const router = useRouter();
    let _useAppContext = useAppContext();
    const [collapsed, setCollapsed] = useState(false)

    useEffect(()=>{
      mainFunc();
    },[props])
  
    const mainFunc = () => {
      const user = getUser();
      if(user['profile'] === null) {
        router.push("/login");
      } else {
        if(user['profile']['username']['firstName'] !==  _useAppContext.user['data']['firstName']){
            _useAppContext.user.update({
                role: user['profile']['username']['role']['roleName'],
                firstName: user['profile']['username']['firstName'],
                lastName: user['profile']['username']['lastName'],
                id: user['profile']['username']['id']
            });
            console.log("main update user");
        }

      }
    }
    
    const toggle = () => {
        setCollapsed(!collapsed)
    };

    const logout = () => {
        signOut();
        window.location.href = "/";
    }

    const openKeys = (_params) => {
        switch (_params) {
          case "importation":
            return "buyRice"
            break;
          case "importation":
            return "rice"
            break;
        
          default:
            return ""
            break;
        }
      }

    return (
        <div>
            <Layout id="layout-desktop">

                <Drawer
                    id="sider-mobile"
                    title={
                    <div
                        style={{
                        color: "#fff",
                        textAlign: "center",
                        fontWeight: 800,
                        fontSize: 19,
                        backgroundColor: '#16C09F',
                        borderRadius: 12,
                        padding:'0.3rem'
                        }}
                    >
                        สหบูรณ์ข้าวไทย
                    </div>
                    }
                    placement="left"
                    closable={false}
                    onClose={()=>{toggle()}}
                    visible={collapsed}
                    key="left"
                >
                    <Menu
                    defaultSelectedKeys={[router.asPath.replace('/','')]}
                    mode="inline"
                    style={{ 
                        backgroundColor: "#ffffff",
                        borderRight: 'none'
                    }} 
                    >
                        <SubMenu key="importation" icon={<ArrowRightOutlined />} title="รับเข้าข้าว">
                            <Menu.Item
                                style={{ marginTop: 10, marginBottom: 10, height: 42 }}
                                key="buyRice"
                                icon={<span>●</span>}
                            >
                                <Link href="/buyRice"><a className="mylink">ข้าวเปลือก</a></Link>
                            </Menu.Item>
                            <Menu.Item
                                style={{ marginTop: 10, marginBottom: 10, height: 42 }}
                                key="rice"
                                icon={<span>●</span>}
                            >
                                <Link href="/rice"><a className="mylink">ข้าวสาร</a></Link>
                            </Menu.Item>
                        </SubMenu>

                        <SubMenu key="cutout" icon={<ArrowLeftOutlined />} title="จำหน่ายข้าว">
                            <Menu.Item
                                style={{ marginTop: 10, marginBottom: 10, height: 42 }}
                                key="cuttingStock"
                                icon={<span>●</span>}
                            >
                                <Link href="/cuttingStock"><a className="mylink">ข้าวเปลือก</a></Link>
                            </Menu.Item>
                            <Menu.Item
                                style={{ marginTop: 10, marginBottom: 10, height: 42 }}
                                key="cuttingStockRice"
                                icon={<span>●</span>}
                            >
                                <Link href="/cuttingStockRice"><a className="mylink">ข้าวสาร</a></Link>
                            </Menu.Item>
                        </SubMenu>

                        <Menu.Item
                            style={{ marginTop: 10, marginBottom: 10, height: 42 }}
                            key="store"
                            icon={<PieChartOutlined />}
                        >
                            <Link href="/store"><a className="mylink">สินค้าคงคลัง</a></Link>
                        </Menu.Item>

                        <Menu.Item className="mylink" style={{marginTop: 10, marginBottom: 10, height: 42}} key="totalImportedRice" icon={<BarChartOutlined />}>
                            <Link className="mylink" href="/report/importedRice">สรุปยอด รับเข้าข้าว</Link> 
                        </Menu.Item>

                        
                        <SubMenu key="summarize" icon={<DatabaseOutlined />} title="สรุปยอด">
                            <Menu.Item
                                style={{ marginTop: 10, marginBottom: 10, height: 42 }}
                                key="summarizePaddy"
                                icon={<span>●</span>}
                            >
                                <Link href="/summarizePaddy"><a className="mylink">ข้าวเปลือก</a></Link>
                            </Menu.Item>
                            <Menu.Item
                                style={{ marginTop: 10, marginBottom: 10, height: 42 }}
                                key="summarizeRices"
                                icon={<span>●</span>}
                            >
                                <Link href="/summarizeRices"><a className="mylink">ข้าวสาร</a></Link>
                            </Menu.Item>
                        </SubMenu>

                        {( (_useAppContext['user']['data']['role'] !== 'employee' && _useAppContext['user']['data']['role'] !== null ) && 
                            <SubMenu key="setting" icon={<SettingOutlined />} title="ตั้งค่า">
                                <Menu.Item className="mylink" style={{marginTop: 10, marginBottom: 10, height: 42}} icon={<span>●</span>} key="employee">
                                    <Link className="mylink" href="/employee">จัดการพนักงาน</Link>
                                </Menu.Item>
                                <Menu.Item className="mylink" style={{marginTop: 10, marginBottom: 10, height: 42}} icon={<span>●</span>} key="riceType">
                                    <Link className="mylink" href="/riceType">ชนิดข้าว</Link>
                                </Menu.Item>
                                <Menu.Item  className="mylink"style={{marginTop: 10, marginBottom: 10, height: 42}} icon={<span>●</span>} key="place">
                                    <Link className="mylink" href="/place">สถานที่เก็บ</Link>
                                </Menu.Item>
                            </SubMenu>
                        )}
                        
                        <Menu.Item
                            onClick={() => {
                            logout();
                            }}
                            style={{ marginTop: 10, marginBottom: 10, height: 42 }}
                            key="logout"
                            icon={<LogoutOutlined />}
                        >
                            ออกจากระบบ
                        </Menu.Item>
                    </Menu>
                </Drawer>

                <Sider
                    id="sider-desktop"
                    style={{backgroundColor: "#383838"}} 
                    trigger={null} 
                    collapsible 
                    collapsed={collapsed}
                >
                    <div className="logo">
                        <div style={{padding: '10px 6px 10px 6px', backgroundColor: "#383838", lineHeight:2}}>
                            {collapsed ?  
                                <center>
                                    <a href="/"><img style={{width:42, height:38}} src="/logo.png"/></a>
                                </center>
                                :
                                <div style={{color: '#fff', textAlign: 'center', fontWeight: 700, backgroundColor: 'rgb(22, 192, 159)', fontSize: 19, borderRadius: 12}}><a className="mylink" href="/">สหบูรณ์ข้าวไทย</a></div>
                            }
                        </div>
                    </div>
                    <Menu style={{backgroundColor: "#383838"}} mode="inline" defaultSelectedKeys={["1"]}>
                        
                        <SubMenu key="importation" icon={<ArrowRightOutlined />} title="รับเข้าข้าว">
                            <Menu.Item className="mylink" style={{marginTop: 10, marginBottom: 10, height: 42}} key="buyRice" icon={<span>●</span>}>
                                <a className="mylink" href="/buyRice">ข้าวเปลือก</a> 
                            </Menu.Item>

                            <Menu.Item className="mylink" style={{marginTop: 10, marginBottom: 10, height: 42}} key="rice" icon={<span>●</span>}>
                                <a className="mylink" href="/rice">ข้าวสาร</a> 
                            </Menu.Item>
                        </SubMenu>

                        <SubMenu key="cutout" icon={<ArrowLeftOutlined />} title="จำหน่ายข้าว">
                            <Menu.Item className="mylink" style={{marginTop: 10, marginBottom: 10, height: 42}} key="riceStock" icon={<span>●</span>}>
                                <a className="mylink" href="/cuttingStock">ข้าวเปลือก</a> 
                            </Menu.Item>
                            <Menu.Item className="mylink" style={{marginTop: 10, marginBottom: 10, height: 42}} key="cuttingStockRice" icon={<span>●</span>}>
                                <a className="mylink" href="/cuttingStockRice">ข้าวสาร</a> 
                            </Menu.Item>
                        </SubMenu>

                        <Menu.Item className="mylink" style={{marginTop: 10, marginBottom: 10, height: 42}} key="store" icon={<PieChartOutlined />}>
                            <a className="mylink" href="/store">สินค้าคงคลัง</a> 
                        </Menu.Item>

                        <Menu.Item className="mylink" style={{marginTop: 10, marginBottom: 10, height: 42}} key="totalImportedRice" icon={<BarChartOutlined />}>
                            <a className="mylink" href="/report/importedRice">สรุปยอด รับเข้าข้าว</a> 
                        </Menu.Item>

                        <SubMenu key="summarize" icon={<DatabaseOutlined />} title="สรุปยอด">
                            <Menu.Item className="mylink" style={{marginTop: 10, marginBottom: 10, height: 42}} key="summarizePaddy" icon={<span>●</span>}>
                                <a className="mylink" href="/summarizePaddy">ข้าวเปลือก</a> 
                            </Menu.Item>

                            <Menu.Item className="mylink" style={{marginTop: 10, marginBottom: 10, height: 42}} key="summarizeRices" icon={<span>●</span>}>
                                <a className="mylink" href="/summarizeRices">ข้าวสาร</a> 
                            </Menu.Item>
                        </SubMenu>

                        {( (_useAppContext['user']['data']['role'] !== 'employee' && _useAppContext['user']['data']['role'] !== null ) && 
                            <SubMenu key="setting" icon={<SettingOutlined />} title="ตั้งค่า">
                                <Menu.Item className="mylink" style={{marginTop: 10, marginBottom: 10, height: 42}} icon={<span>●</span>} key="employee">
                                    <a className="mylink" href="/employee">จัดการพนักงาน</a>
                                </Menu.Item>
                                <Menu.Item className="mylink" style={{marginTop: 10, marginBottom: 10, height: 42}} icon={<span>●</span>} key="riceType">
                                    <a className="mylink" href="/riceType">ชนิดข้าว</a>
                                </Menu.Item>
                                <Menu.Item  className="mylink"style={{marginTop: 10, marginBottom: 10, height: 42}} icon={<span>●</span>} key="place">
                                    <a className="mylink" href="/place">สถานที่เก็บ</a>
                                </Menu.Item>
                            </SubMenu>
                        )}
                        
                        <Menu.Item className="mylink-red" onClick={()=>{logout();}} style={{marginTop: 10, marginBottom: 10, height: 42}} key="logout" icon={<LogoutOutlined />}>
                            ออกจากระบบ
                        </Menu.Item>
                    </Menu>
                </Sider>
                <Layout className="site-layout">
                    <Header className="site-layout-background">
                        <div style={{ paddingTop: '1px'}} >
                            {collapsed ? <MenuUnfoldOutlined  style={{color:'#16c09f'}} onClick={()=>{toggle()}} /> : <MenuFoldOutlined style={{color:'#16c09f'}} onClick={()=>{toggle()}}/> }
                            <div style={{float:'right', color:'#fff', width:'fit-content'}}><strong style={{borderBottom:"3px solid #16c09f"}} >{`${_useAppContext['user']['data']['firstName']} ${_useAppContext['user']['data']['lastName']}`}</strong></div>
                        </div>
                    </Header>
                <Content style={{ minHeight:"100vh", maxHeight: "100%"}}>
                    <div style={{
                        margin: "28px 20px",
                        padding: 24,
                        minHeight: 400,
                        backgroundColor:"#dedede",
                        borderRadius: 10,
                        boxShadow: "5px 10px 22px #888888"
                    }}>
                        {props.children}
                    </div>
                </Content>
                </Layout>
            </Layout>
        </div>
    );

}

export default MainMenu;
