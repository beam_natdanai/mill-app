import { Form, Input, Button, Checkbox, Avatar, Image } from "antd";
import { UserOutlined, LockOutlined, AntDesignOutlined } from "@ant-design/icons";

const NormalLoginForm = ({callback}) => {
  
  const onFinish = (values) => {
    callback({data: values});
  };

  return (
    <div className="vh100 vertical-center section">
        <section>
          <div className="container" style={{width:400}} >
            <div className="row">
              <div className="col-12" style={{textAlign:'center'}}>
                  <h1>ระบบ สหบูรณ์ข้าวไทย</h1>
              </div>
            </div>
            <div className="row">
              <div className="col-12 section">
                <div style={{width: 'fit-content',backgroundColor:'#16c09f',borderRadius:'20%', textAlign:'center'}}>
                    <Avatar
                      style={{margin:'3rem'}}
                      shape="square"
                      size={{ xs: 64, sm: 64, md: 64, lg: 80, xl: 120, xxl: 160 }}
                      src={<Image src="logo.png" />}
                    />
                </div>
              </div>
            </div>
            <div className="row">
                <div className="col-12" style={{marginTop:'2rem'}}>
                  <Form
                    name="normal_login"
                    className="login-form"
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                  >
                    <Form.Item
                      name="username"
                      rules={[{ required: true, message: "Please input your Username!" }]}
                    >
                      <Input
                        prefix={<UserOutlined className="site-form-item-icon" />}
                        placeholder="Username"
                      />
                    </Form.Item>
                    <Form.Item
                      name="password"
                      rules={[{ required: true, message: "Please input your Password!" }]}
                    >
                      <Input
                        prefix={<LockOutlined className="site-form-item-icon" />}
                        type="password"
                        placeholder="Password"
                      />
                    </Form.Item>
                    <Form.Item>
                      <Button block type="primary" htmlType="submit" className="login-form-button">
                        เข้าสู่ระบบ
                      </Button>
                    </Form.Item>
                  </Form>
                </div>
            </div>
          </div>
        </section>
        <style jsx>{`
            #components-form-demo-normal-login .login-form {
              max-width: 300px;
            }
            #components-form-demo-normal-login .login-form-forgot {
              float: right;
            }
            #components-form-demo-normal-login .ant-col-rtl .login-form-forgot {
              float: left;
            }
            #components-form-demo-normal-login .login-form-button {
              width: 100%;
            }
        `}</style>
    </div>
  );
};

export default NormalLoginForm;
