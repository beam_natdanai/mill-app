import Head from 'next/head';
import axios from 'axios';
import  { useEffect } from 'react';
import { useRouter } from 'next/router';
import LoginForm from '../components/formSignin';
import { signIn, getUser } from '../lib/user';


function Login(props){

    const router = useRouter();

    useEffect(()=>{
        mainFunc();
    },[props]);
    
    const mainFunc = () => {
        const user = getUser();
        if(user['profile'] !== null) {
            router.push("/");
        }
    };

    const funcLogin = async (_username, _password) => {
        try {
            const response = await axios.post('http://139.59.247.219:8000/auth/login',{username: _username, password: _password});
            return ({status: true, response: response});
        } catch (error) {
            return ({status: false, response: error});
        }
    }

    const loginCallback = async (callback) => {
        
        try {

            const resp = await funcLogin(callback['data']['username'], callback['data']['password']);
            if(resp['status']) {
                signIn(resp.response.data, resp.response.data.role);
                router.push("/");
            } else {
                alert(" username และ password ไม่ตรงกัน กรุณาตรวจอีกครั้ง !");
            }
        } catch (error) {
            console.log("error ! ",error);
        }
        
    }

    return (
        <div>
            <Head>
            <title>สหบูรณ์ข้าวไทย - เข้าสู่ระบบ</title>
            </Head>
            <div style={{height:"100vh"}}>
                <LoginForm callback={loginCallback} />
            </div>
        </div>
    );
}

export default Login;