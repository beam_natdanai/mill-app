import Head from "next/head";
import axios from "axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Table, DatePicker } from "antd";
import { getUser } from "../../lib/user.js";
import MainMenu from "../../components/layout/main";
import { useAppContext } from "../../context/state";
import {
  Select,
  Space,
  Form,
  Button,
  InputNumber,
  Input,
  Spin,
  Divider,
  Card,
  Row,
  Col
} from "antd";
import moment from "moment";
import "moment/locale/th";
import locale from "antd/lib/locale/th_TH";
const { Option } = Select;
const { TextArea } = Input;

function summarizePaddy(props) {
  const router = useRouter();
  const [spin, setSpine] = useState(true);
  const [accountStore, setAccountStore] = useState(null);
  let _useAppContext = useAppContext();
  
  const millType = [
    {
      id: 1,
      name: "start",
      desc: "ข้าวต้น"
    },
    {
      id: 2,
      name: "mid",
      desc: "ข้าวท่อน"
    },
    {
      id: 3,
      name: "final1",
      desc: "ข้าวปลาย 1"
    },
    {
      id: 4,
      name: "final2",
      desc: "ข้าวปลาย 2"
    },
    {
      id: 5,
      name: "machine",
      desc: "ข้าวหลังเครื่อง"
    }
  ]
  
  useEffect(() => {
    mainFunc();
  }, []);

  const mainFunc = () => {
    const user = getUser();
    if (user["profile"] === null) {
      router.push("/login");
    } else {
      init();
      setSpine(false);
    }
  };

  const init = async () => {
    const response = await fetch(
      `http://139.59.247.219:8000/account-store-rices?limit=${props.riceType.count}&sort=id,DESC`
    ).then((r) => r.json());
    // console.log("response.data -> ",response.data);
    if (!response.statusCode) {
      setAccountStore(response);
    } else {
      setAccountStore({ data: [] });
    }
  };

  const menuCallback = (param) => {
    console.log("param -> ", param);
    router.push(`/${param}`);
  };
  
  const onSave = async (_value) => {
    const dateOld = (accountStore.count === 0 ? moment().format("YYYY-MM-DD") : moment(accountStore.data[0]['currentDate']).format("YYYY-MM-DD") ) ;
    const dateNew = _value.currentDate.format("YYYY-MM-DD");
    
    if(accountStore.count === 0 || moment(dateOld).isBefore(dateNew) ){
      setSpine(true);
      const obj = millType.map((row, index)=>{
        return {
          importationSumIn: 0,
          cuttingSumIn: 0,
          importationSumOut: parseFloat(_value[`importation_${row.name}`]),
          cuttingSumOut: parseFloat(_value[`cutting_${row.name}`]),
          balance: parseFloat(_value[`balance_${row.name}`]),
          description: _value['description'],
          currentDate: moment(_value.currentDate).format("YYYY-MM-DD HH:mm:ss"),
          day:  moment(_value.currentDate).format("DD"),
          month:  moment(_value.currentDate).format("MM"),
          year: moment(_value.currentDate).format("YYYY"),
          activeStatus: "active",
          millType: row.name
        };
      });
      // console.log("obj -> ",obj);
      const response = await Promise.all(obj.map((row, index)=>{
        return fetch(`http://139.59.247.219:8000/account-store-rices`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(row),
        }).then((r) => r.json())
      }));

      if (response.error) {
        alert("กรุณาตรวจสอบข้อมูลให้ถูกต้อง !");
        setSpine(false);
      } else {
        alert("เพิ่มข้อมูลเสร็จสิ้น");
        window.document.getElementById("addRiceReset").click();
        setSpine(false);
      }
    } else {
      alert(`วันที่ ${moment(_value.currentDate).format("DD/MM/YYYY")} มีข้อมูลอยู่แล้ว`);
    }

  };

  return (
    <div>
      <Head>
        <title>สหบูรณ์ข้าวไทย - สรุปยอด ข้าวสาร</title>
      </Head>
      <div>
        <MainMenu callback={menuCallback}>
          <Spin spinning={spin} tip="กำลังโหลด...">

            <Row justify="center" style={{ marginTop: "1rem" }}>
              <Col span={24}>
                <h2>สรุปยอด ข้าวสาร</h2>
              </Col>
            </Row>
    
            <Row justify="center" gutter={16} style={{ marginTop: "1rem" }}>
              <Col xs={24} sm={24} md={12} lg={12} >
                {accountStore !== null &&
                  (accountStore.count === 0 ? (
                    
                      <Form
                        name="oldForm"
                        layout={"vertical"}
                        initialValues={Object.assign({currentDate: moment().add(-1, "days").startOf("day")},...millType.map((row, index)=>{
                          return {
                            [`importation_${row.name}`]: 0,
                            [`cutting_${row.name}`]: 0,
                            [`balance_${row.name}`]: 0
                          }
                        }))}
                      >
                        <Form.Item
                          label="ยอดยกมา วันที่"
                          name="currentDate"
                          rules={[{ required: false }]}
                        >
                          <DatePicker
                            disabled
                            style={{ maxWidth: 400, width: '100%' }}
                            locale={locale}
                            format="YYYY-MM-DD"
                            value={moment()}
                            placeholder="วันที่สรุปยอด"
                          />
                        </Form.Item>

                        {millType.map((res, index) => {
                          return (
                            <Card
                              key={`millType${index}`}
                              style={{ maxWidth: 400, margin: ".5rem .2rem" }}
                            >
                              <Divider
                                orientation="left"
                                plain
                                style={{ color: "#666666" }}
                              >
                                {`ยอดยกมา ${res.desc}`}
                              </Divider>
                              <Form.Item
                                label="ยอดยกมา รับเข้า"
                                name={`importation_${res.name}`}
                                rules={[{ required: false }]}
                              >
                                <Input
                                  disabled
                                  type="number"
                                  placeholder="น้ำหนัก / กก."
                                  min="1"
                                  max="5000"
                                  style={{ width: "100%", maxWidth: 400 }}
                                />
                              </Form.Item>

                              <Form.Item
                                label="ยอดยกมา จำหน่ายออก"
                                name={`cutting_${res.name}`}
                                rules={[{ required: false }]}
                              >
                                <Input
                                  disabled
                                  type="number"
                                  placeholder="น้ำหนัก / กก."
                                  min="1"
                                  max="5000"
                                  style={{ width: "100%", maxWidth: 400 }}
                                />
                              </Form.Item>

                              <Form.Item
                                label="ยอดยกมา คงเหลือ"
                                name={`balance_${res.name}`}
                                rules={[{ required: false }]}
                              >
                                <Input
                                  disabled
                                  type="number"
                                  placeholder="น้ำหนัก / กก."
                                  min="1"
                                  max="5000"
                                  style={{ width: "100%", maxWidth: 400 }}
                                />
                              </Form.Item>
                            </Card>
                          );
                        })}

                        <Form.Item
                          label="รายละเอียด"
                          name="description"
                          rules={[{ required: false }]}
                        >
                          <TextArea
                            disabled
                            placeholder="หมายเหตุ"
                            rows={3}
                            style={{ width: '100%', maxWidth: 400 }}
                          />
                        </Form.Item>

                        {/* <Form.Item>
                          <Button
                            style={{ margin: "0.5rem" }}
                            type="primary"
                            htmlType="submit"
                          >
                            บันทึก
                          </Button>

                          <Button
                            style={{ margin: "0.5rem" }}
                            id="addRiceReset"
                            type="danger"
                            htmlType="Reset"
                            onClick={() => {
                              router.push("/summarizePaddy");
                            }}
                          >
                            ยกเลิก
                          </Button>
                        </Form.Item> */}
                      </Form>
                  
                  ) : (
                    // <div className="row">
                    //   <div className="col-12">
                    //     <h3>
                    //       <text style={{ color: "#16c09f" }}>ยอดยกมา</text>{" "}
                    //       วันที่:{" "}
                    //       {moment()
                    //         .add(-1, "days")
                    //         .startOf("day")
                    //         .format("YYYY-MM-DD HH:mm:ss")}
                    //     </h3>
                    //   </div>
                    // </div>
                    <Form
                      name="oldForm"
                      layout={"vertical"}
                      initialValues={Object.assign({
                        currentDate: (accountStore !== null ? moment(accountStore.data[0]['currentDate']) : moment().add(-1, "days").startOf("day") )},
                        ...props.riceType.data.map((row, index)=>{
                        return {
                          [`importation_${row.id}`]: accountStore.data[index]['importationSumOut'],
                          [`cutting_${row.id}`]: accountStore.data[index]['cuttingSumOut'],
                          [`balance_${row.id}`]: accountStore.data[index]['balance']
                        }
                      }))}
                    >
                      <Form.Item
                        label="ยอดยกมา วันที่"
                        name="currentDate"
                        rules={[{ required: false }]}
                      >
                        <DatePicker
                          disabled
                          style={{ width: '100%', maxWidth: 400 }}
                          locale={locale}
                          format="YYYY-MM-DD"
                          value={(accountStore !== null && moment().format("YYYY-MM-DD HH:mm:ss"))}
                          placeholder="วันที่สรุปยอด"
                        />
                      </Form.Item>

                      {millType.map((res, index) => {
                        return (
                          <Card
                            key={`riceType${index}`}
                            style={{ maxWidth: 400, margin: ".5rem .2rem" }}
                          >
                            <Divider
                              orientation="left"
                              plain
                              style={{ color: "#666666" }}
                            >
                              {`ยอดยกมา ${res.desc}`}
                            </Divider>
                            <Form.Item
                              label="ยอดยกมา รับเข้า"
                              name={`importation_${res.id}`}
                              rules={[{ required: false }]}
                            >
                              <Input
                                disabled
                                type="number"
                                placeholder="น้ำหนัก / กก."
                                min="1"
                                max="5000"
                                style={{ width: "100%", maxWidth: 400 }}
                              />
                            </Form.Item>

                            <Form.Item
                              label="ยอดยกมา จำหน่ายออก"
                              name={`cutting_${res.id}`}
                              rules={[{ required: false }]}
                            >
                              <Input
                                disabled
                                type="number"
                                placeholder="น้ำหนัก / กก."
                                min="1"
                                max="5000"
                                style={{ width: "100%", maxWidth: 400 }}
                              />
                            </Form.Item>

                            <Form.Item
                              label="ยอดยกมา คงเหลือ"
                              name={`balance_${res.id}`}
                              rules={[{ required: false }]}
                            >
                              <Input
                                disabled
                                type="number"
                                placeholder="น้ำหนัก / กก."
                                min="1"
                                max="5000"
                                style={{ width: "100%", maxWidth: 400 }}
                              />
                            </Form.Item>
                          </Card>
                        );
                      })}

                      <Form.Item
                        label="รายละเอียด"
                        name="description"
                        rules={[{ required: false }]}
                      >
                        <TextArea
                          disabled
                          placeholder="หมายเหตุ"
                          rows={3}
                          style={{ width: "100%", maxWidth: 400 }}
                        />
                      </Form.Item>
                    </Form>
                  ))}
              </Col>

              <Col xs={24} sm={12} md={12} lg={12} >
                <Form name="addPlaces" onFinish={onSave} layout={"vertical"}>
                  <Form.Item
                    label="วันที่สรุปยอด"
                    name="currentDate"
                    rules={[{ required: true }]}
                  >
                    <DatePicker
                      style={{ width: "100%", maxWidth: 400 }}
                      locale={locale}
                      format="YYYY-MM-DD"
                      value={moment()}
                      placeholder="วันที่สรุปยอด"
                    />
                  </Form.Item>

                  {millType.map((res, index) => {
                    return (
                      <Card
                        key={`riceType${index}`}
                        style={{ maxWidth: 400, margin: ".5rem .2rem" }}
                      >
                        <Divider
                          orientation="left"
                          plain
                          style={{ color: "#666666" }}
                        >
                          {res.desc}
                        </Divider>
                        <Form.Item
                          label="รับเข้า"
                          name={`importation_${res.name}`}
                          rules={[{ required: true }]}
                        >
                          <Input
                            type="number"
                            placeholder="น้ำหนัก / กก."
                            min="1"
                            max="5000"
                            style={{ width: "100%", maxWidth: 400 }}
                          />
                        </Form.Item>

                        <Form.Item
                          label="จำหน่ายออก"
                          name={`cutting_${res.name}`}
                          rules={[{ required: true }]}
                        >
                          <Input
                            type="number"
                            placeholder="น้ำหนัก / กก."
                            min="1"
                            max="5000"
                            style={{ width: "100%", maxWidth: 400 }}
                          />
                        </Form.Item>

                        <Form.Item
                          label="คงเหลือ"
                          name={`balance_${res.name}`}
                          rules={[{ required: true }]}
                        >
                          <Input
                            type="number"
                            placeholder="น้ำหนัก / กก."
                            min="1"
                            max="5000"
                            style={{ width: "100%", maxWidth: 400 }}
                          />
                        </Form.Item>
                      </Card>
                    );
                  })}

                  <Form.Item
                    label="รายละเอียด"
                    name="description"
                    rules={[{ required: false }]}
                  >
                    <TextArea
                      placeholder="หมายเหตุ"
                      rows={3}
                      style={{ width: "100%", maxWidth: 400 }}
                    />
                  </Form.Item>

                  <Form.Item>
                    <Row gutter={8} justify="center">
                      <Col xs={0} sm={24}>
                        <center>
                          <Space>
                            <Button
                              style={{ margin: "0.5rem" }}
                              type="primary"
                              htmlType="submit"
                            >
                              บันทึก
                            </Button>

                            <Button
                              style={{ margin: "0.5rem" }}
                              id="addRiceReset"
                              type="danger"
                              htmlType="Reset"
                              onClick={() => {
                                router.push("/summarizeRices");
                              }}
                            >
                              ยกเลิก
                            </Button>
                          </Space>
                        </center>
                      </Col>

                      <Col xs={24} sm={0}>
                        <Button
                          style={{ margin: "0.2rem 0rem", width: "100%" }}
                          type="danger"
                          id="addRiceReset"
                          htmlType="Reset"
                          block
                          onClick={() => {
                            router.push("/summarizePaddy");
                          }}
                        >
                          ยกเลิก
                        </Button>

                        <Button
                          style={{ margin: "0.2rem 0rem" }}
                          type="primary"
                          htmlType="submit"
                          block
                        >
                          บันทึก
                        </Button>
                      </Col>
                    </Row>
                        
                  </Form.Item>
                </Form>
              </Col>
            </Row>
          </Spin>
        </MainMenu>
      </div>
    </div>
  );
}

export default summarizePaddy;

export async function getServerSideProps(context) {
  const [riceType] = await Promise.all([
    fetch(`http://139.59.247.219:8000/rice-type`).then((r) => r.json()),
  ]);

  return {
    props: {
      riceType: riceType,
    },
  };
}
