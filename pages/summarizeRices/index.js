import { useState } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import  { useEffect } from 'react';
import { Table, Space, Spin } from 'antd';
import { getUser } from '../../lib/user.js';
import MainMenu from "../../components/layout/main";
import { 
  Button,
  Form, 
  Row, 
  Col, 
  Divider,
  DatePicker,
  Tooltip,
  Typography,
  InputNumber,
  Modal } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import moment from 'moment';
import 'moment/locale/th';
import locale from 'antd/lib/locale/th_TH';
const { Title, Text } = Typography;

function RiceTypeList(props){

  const [form] = Form.useForm();
  const router = useRouter();
  const [spin, setSpine] = useState(true);
  const [startValue,setStartValue] = useState(null);
  const [endValue,setEndValue] = useState(null);
  const [endOpen,setEndOpen] = useState(false);
  const [dateStart,setDateStart] = useState(moment().startOf('day'));
  const [dateEnd,setDateEnd] = useState(moment().endOf('day'));
  const [dataTable, setDataTable] = useState({data:[]});
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [rowSelect, setRowSelect] = useState(null);

  const millType = {
    start: "ข้าวต้น",
    mid: "ข้าวท่อน",
    final1: "ข้าวปลาย 1",
    final2: "ข้าวปลาย 2",
    machine: "ข้าวหลังเครื่อง"
  }
  
  const columns = [
    {
      title: 'ลำดับ',
      dataIndex: 'id',
      align: 'center',
      width:70,
      render:(text, record, index)=>{
        return (record['activeStatus'] === 'inactive'? <font color="#e35349" size="2">{index + 1}</font> : <font size="2">{index + 1}</font> )
      }
    },
    {
      title: 'วันที่',
      dataIndex: 'currentDate',
      align: 'center',
      width: 150,
      render:(text, record, index)=>{
        return (record['activeStatus'] === 'inactive'? <font color="#e35349" size="2">{moment(text).format('YYYY-MM-DD HH:mm:ss')}</font> : <font size="2">{moment(text).format('YYYY-MM-DD HH:mm:ss')}</font> ) 
      }
    },
    {
      title: 'ประเภทข้าว',
      dataIndex: 'millType',
      align: 'center',
      width: 100,
      render:(text, record, index)=>{
        return (record['activeStatus'] === 'inactive'? <font color="#e35349" size="2">{millType[text]}</font> : <font size="2">{millType[text]}</font> )
      }
    },
    {
      title: 'รับเข้าข้าว กก.',
      dataIndex: 'importationSumOut',
      align: 'center',
      width: 100,
      render:(text, record, index)=>{
        return (record['activeStatus'] === 'inactive'? <font color="#e35349" size="2">{text}</font> : <font size="2">{text}</font> )
      }
    },
    {
      title: 'จำหน่าย กก.',
      dataIndex: 'cuttingSumOut',
      align: 'center',
      width: 100,
      render:(text, record, index)=>{
        return (record['activeStatus'] === 'inactive'? <font color="#e35349" size="2">{text}</font> : <font size="2">{text}</font> )
      }
    },
    {
      title: 'คงเหลือ',
      dataIndex: 'balance',
      align: 'center',
      width: 100,
      render:(text, record, index)=>{
        return (record['activeStatus'] === 'inactive'? <font color="#e35349" size="2">{text}</font> : <font size="2">{text}</font> )
      }
    },
    {
      title: 'รายละเอียด',
      dataIndex: 'description',
      align: 'center',
      width: 200,
      render:(text, record, index)=>{
        return (record['activeStatus'] === 'inactive'? <font color="#e35349" size="2">{text}</font> : <font size="2">{text}</font> )
      }
    },
    {
      title: 'จัดการ',
      dataIndex: 'id',
      align: 'center',
      width: 130,
      render:(text, record, index)=>{
          return (record['activeStatus'] === 'inactive'?  
            <Space>
              <Button 
              size="small" 
              shape="round" 
              style={{margin:'0.5rem'}} 
              className="hover-warning" 
              onClick={() => {
                showModal(record);
              }}>
                แก้ไข
              </Button>
              {/* <Button size="small" shape="round" style={{margin:'0.5rem'}} type="danger" onClick={()=>{handleDelete(record)}} >ลบ</Button> */}
            </Space>
          :
            <Space>
              <Button 
              size="small" 
              shape="round" 
              style={{margin:'0.5rem'}} 
              className="hover-warning" 
              onClick={() => {
                showModal(record);
              }}>
                แก้ไข
              </Button>
              {/* <Button size="small" shape="round" style={{margin:'0.5rem'}} type="danger" onClick={()=>{handleDelete(record)}} >ลบ</Button> */}
            </Space>
          )
      }
    },
  ];

  useEffect(()=>{
    mainFunc();
  },[]);

  const mainFunc = () => {
    const user = getUser();
    if(user['profile'] === null) {
      router.push("/login");
    }else{
      init();
      setSpine(false);
    }
  }

  const init = async () => {
    const response = await fetch(`http://139.59.247.219:8000/account-store-rices?filter=currentDate||$gte||${dateStart.format("YYYY-MM-DD HH:mm:ss")}&filter=currentDate||$lte||${dateEnd.format("YYYY-MM-DD HH:mm:ss")}&sort=currentDate,ASC`).then(r => r.json());
    if(!response.statusCode){
      setDataTable(response);
    } else {
      setDataTable({data:[]});
    }
  }

  const menuCallback = (param) => {
    console.log("param -> ",param);
  }

  // const handleDelete = async ({id, currentDate}) => {
  //   let resultConfirm = confirm(`คุณกำลังจะลบข้อมูล วันที่ "${moment(currentDate).format('YYYY-MM-DD HH:mm:ss')}" ใช่หรือไม่ ?`);
  //   if(resultConfirm){
  //     setSpine(true);
  //     const response = await fetch(`http://139.59.247.219:8000/account-store-rices/${id}`,{
  //       method: 'PATCH',
  //       headers: {
  //         'Content-Type': 'application/json'
  //       },
  //       body: JSON.stringify({activeStatus:"inactive"})
  //     }).then(r => r);
  //     if(response.statusCode){
  //         alert("เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง !");
  //         setSpine(false);
  //     }else{
  //         alert(`ลบข้อมูล วันที่ "${currentDate}" เสร็จสิ้น`);
  //         router.push("/summarizeRices");
  //         setSpine(false);
  //     }

  //   }
  // }

  const handleDelete = async ({id}) => {

    let resultConfirm = confirm(`คุณกำลังจะลบ ข้อมูลนี้ ใช่หรือไม่ ?`);
    if(resultConfirm){

      setSpine(true);
      const response = await fetch(`http://139.59.247.219:8000/account-store-rices/${id}`,{
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json'
        },
        body: null
      }).then(r => r);

      if(response.error){
          alert("เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง !");
          setSpine(false);
      }else{
          alert(`ลบข้อมูล เสร็จสิ้น`);
          router.push("/summarizeRices");
          init();
          setSpine(false);
      }

    }
  }

  const handleSearch = async () => {

    const response = await fetch(`http://139.59.247.219:8000/account-store-rices?filter=currentDate||$gte||${dateStart.format("YYYY-MM-DD HH:mm:ss")}&filter=currentDate||$lte||${dateEnd.format("YYYY-MM-DD HH:mm:ss")}&sort=currentDate,ASC`).then(r => r.json());
    if(!response.statusCode){
      setDataTable(response);
    }

  };

  const handleStartOpenChange = open => {
    if (!open) {
      setEndOpen(true);
    }
  };

  const handleEndOpenChange = open => {
    setEndOpen(open);
  };

  const showModal = (_record) => {
    form.setFieldsValue({
      importation: _record.importationSumOut,
      cutting: _record.cuttingSumOut,
      balance: _record.balance,
      currentDate: moment(_record.currentDate),
    });
    setRowSelect(_record);
    setIsModalVisible(true);
  };

  const handleOk = async () => {

    const obj = {
      importationSumOut: form.getFieldValue("importation"),
      cuttingSumOut: form.getFieldValue("cutting"),
      balance: form.getFieldValue("balance"),
      currentDate: moment(form.getFieldValue("currentDate")),
      day:  moment(form.getFieldValue("currentDate")).format("DD"),
      month:  moment(form.getFieldValue("currentDate")).format("MM"),
      year: moment(form.getFieldValue("currentDate")).format("YYYY"),
    }
    
    const response = await fetch(`http://139.59.247.219:8000/account-store-rices/${rowSelect.id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(obj),
    }).then((r) => r.json());

    if (response.error) {
      alert("กรุณาตรวจสอบข้อมูลให้ถูกต้อง !");
    } else {
      setIsModalVisible(false);
      init();
    }

  };

  const handleCancel = () => {
    setRowSelect(null);
    setIsModalVisible(false);
  };

  const onSaveUpdate = () => {
    
  };

  return (
    <div>
      <Head>
        <title>สหบูรณ์ข้าวไทย - สรุปยอด</title>
      </Head>
      <div>
        <MainMenu callback={menuCallback} >
          <Spin  spinning={spin} tip="กำลังโหลด..." >
            <Row>
                <Col span={24}>
                  <div style={{marginTop:'1rem', marginBottom:'1rem'}}><Title level={4}>รายการ สรุปยอดข้าวสาร</Title></div>
                </Col>
            </Row>
            <Row>
                <Col  xs={0} sm={0} md={16} lg={16}>
                  <Space>
                    วันที่เริ่มต้น: 
                    <DatePicker
                      locale={locale}
                      format="YYYY-MM-DD"
                      value={dateStart}
                      placeholder="วันที่เริ่มต้น"
                      onChange={(value)=>{setDateStart(value.startOf('day'))}}
                      onOpenChange={handleStartOpenChange}
                    />
                    วันที่สิ้นสุด: 
                    <DatePicker
                      locale={locale}
                      format="YYYY-MM-DD"
                      value={dateEnd}
                      placeholder="วันที่สิ้นสุด"
                      onChange={(value)=>{setDateEnd(value.endOf('day'))}}
                      onOpenChange={handleEndOpenChange}
                    />
                    <Tooltip title="ค้นหา">
                      <Button shape="circle" type="primary" onClick={()=>{handleSearch()}} icon={<SearchOutlined />} />
                    </Tooltip>
                  </Space>
                </Col>
                <Col xs={0} md={8}>
                    <a href="/summarizeRices/add">
                      <Button style={{float:'right'}} type="primary" shape="round">สรุปยอด ข้าวสาร</Button>
                    </a>
                </Col>
                <Col xs={24} sm={24} md={0} lg={0}>
                  <Row gutter={8}>
                    <Col xs={24} sm={24} md={0}>
                      <DatePicker
                        style={{width:'100%', margin:'0.2rem 0'}}
                        locale={locale}
                        format="YYYY-MM-DD"
                        value={dateStart}
                        placeholder="วันที่เริ่มต้น"
                        onChange={(value)=>{setDateStart(value.startOf('day'))}}
                        onOpenChange={handleStartOpenChange}
                      />
                    </Col>
                    <Col xs={24} sm={24} md={0}>
                      <DatePicker
                        style={{width:'100%', margin:'0.2rem 0'}}
                        locale={locale}
                        format="YYYY-MM-DD"
                        value={dateEnd}
                        placeholder="วันที่สิ้นสุด"
                        onChange={(value)=>{setDateEnd(value.endOf('day'))}}
                        onOpenChange={handleEndOpenChange}
                      />
                    </Col>
                    <Col xs={24} sm={24} md={0}>
                        <Button style={{margin:'0.2rem 0'}} block type="primary" onClick={()=>{handleSearch()}} icon={<span>ค้นหา <SearchOutlined /></span>} />
                    </Col>

                    <Col xs={24} md={0}>
                      <a href="/summarizeRices/add">
                        <Button
                          style={{ margin:'0.2rem 0' }}
                          type="primary"
                          shape="round"
                          block
                        >
                          สรุปยอด ข้าวสาร
                        </Button>
                      </a>
                    </Col>
                  </Row>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col className="gutter-row" span={24}>
                    <Table 
                    style={{marginTop:'1rem', marginBottom:'1rem'}}
                    bordered
                    responsive
                    size="small"
                    columns={columns} 
                    dataSource={dataTable['data'].map((element, index)=>{return Object.assign(element,{key:index});})} 
                    pagination={{ pageSize: 25 }} scroll={{ y: 520 }} />
                </Col>
            </Row>

          </Spin>
        </MainMenu>
        <Modal
          title="แก้ไข สรุปยอดข้าวเปลือก"
          visible={isModalVisible}
          onOk={()=>{handleOk()}}
          onCancel={()=>{handleCancel()}}
          width={320}
        >
            {(rowSelect !== null &&
              <Form name="addPlaces" 
                form={form}
                onFinish={onSaveUpdate}
                layout={"vertical"} 
                initialValues={{
                  currentDate: moment(rowSelect.currentDate),
                  importation: rowSelect.importationSumOut,
                  cutting: rowSelect.cuttingSumOut,
                  balance: rowSelect.balance
                }} 
              >
                <p>{`ชนิด: ${millType[rowSelect.millType]}`}</p>

                <Form.Item
                  label="วันที่สรุปยอด"
                  name="currentDate"
                  rules={[{ required: true }]}
                >
                  <DatePicker
                    style={{ width: '100%' }}
                    locale={locale}
                    format="YYYY-MM-DD"
                    placeholder="วันที่สรุปยอด"
                  />
                </Form.Item>

                <Form.Item
                  label="รับเข้า"
                  name={`importation`}
                  rules={[{ required: true }]}
                >
                  <InputNumber
                    placeholder="น้ำหนัก / กก."
                    style={{
                      width: '100%',
                    }}
                    min={0}
                    max={5000}
                    step={1}
                  />
                </Form.Item>

                <Form.Item
                  label="จำหน่ายออก"
                  name={`cutting`}
                  rules={[{ required: true }]}
                >
                  <InputNumber
                    placeholder="น้ำหนัก / กก."
                    style={{
                      width: '100%',
                    }}
                    min={0}
                    max={5000}
                    step={0}
                  />
                </Form.Item>

                <Form.Item
                  label="คงเหลือ"
                  name={`balance`}
                  rules={[{ required: true }]}
                >
                  <InputNumber
                    placeholder="น้ำหนัก / กก."
                    style={{
                      width: '100%',
                    }}
                    min={0}
                    max={5000}
                    step={1}
                  />
                </Form.Item>
              </Form>
            )}
        </Modal>
      </div>
    </div>
  )
}

export default RiceTypeList;