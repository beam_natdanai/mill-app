import Document, { Html, Head, Main, NextScript } from "next/document";

class MyDocument extends Document {
  render() {
    return (
      <Html lang="th">
        <Head>
          <meta charSet="utf-8" />
          <meta content="text/html; charset=UTF-8" name="Content-Type" />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <link rel="icon" type="image/png" sizes="16x16" href="/logo.png"></link>
        </Head>
        <body className="home">
          <div id="fb-root"></div>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
