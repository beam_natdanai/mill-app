import Head from 'next/head';
import axios from 'axios';
import { useRouter } from 'next/router';
import  { useEffect, useState } from 'react';
import { Table } from 'antd';
import { getUser } from '../../lib/user.js';
import MainMenu from "../../components/layout/main";
import { useAppContext } from '../../context/state';
import { 
  Select, 
  Space, 
  Form,
  Button,
  InputNumber,
  DatePicker,
  Input,
  Spin,
  Row,
  Col } from 'antd';
import moment from 'moment';
import 'moment/locale/th';
import locale from 'antd/lib/locale/th_TH';
const { Option } = Select;
const { TextArea } = Input;

const BuyRiceAdd = (props) => {
  // console.log("props => ",props);
  const router = useRouter();
  const [form] = Form.useForm();
  const [date,setDate] = useState(moment());
  let _useAppContext = useAppContext();
  const [spin, setSpine] = useState(true);
  const [placesSelect, setPlacesSelect] = useState((props['places']['data']['lengt'] === 0 ? null : props['places']['data'][0]));
  const [riceSelect, setRiceSelect] = useState((props['riceType']['data']['lengt'] === 0 ? null : props['riceType']['data'][0]));
  const [ricePrice, setRicePrice] = useState(parseFloat((props['riceType']['data']['lengt'] === 0 ? 0 : props['riceType']['data'][0]['price'])));
  const [riceWeight, setRiceWeight] = useState(0);
  const [netRicePrice, setNetRicePrice] = useState(0);
  
  useEffect(()=>{
    mainFunc();
  },[])

  useEffect(() => {
    form.setFieldsValue({
      ricePrice: ricePrice,
    });
  }, [ricePrice]);

  const mainFunc = () => {
    const user = getUser();
    if(user['profile'] === null) {
      router.push("/login");
    } else {
      setSpine(false);
    }
  }

  const menuCallback = (param) => {
    console.log("param -> ",param);
    router.push(`/${param}`);
  }

  const onSave = async (value) => {

    setSpine(true);
    const obj = {
      carRegistration: value.carRegistration,
      carWeight: 0,
      allWeight: 0,
      netWeight: 0,
      ricePrice: parseFloat(value.ricePrice),
      netRicePrice: (riceWeight * ricePrice),
      name: riceSelect['name'],
      address: placesSelect['address'],
      lot: value.lot,
      billId: value.billId,
      riceWeight: parseFloat(value.riceWeight),
      riceType: value.name,
      places: value.address,
      currentDate: moment(value.curentDate).format('YYYY-MM-DD HH:mm:ss'),
      day: moment(value.curentDate).format('DD'),
      month: moment(value.curentDate).format('MM'),
      year: moment(value.curentDate).format('YYYY'),
      description: value.description,
      user: _useAppContext['user']['data']['id']
    }

    const response = await fetch(`http://139.59.247.219:8000/importation`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(obj)
    }).then(r => r.json());

    if(response.error){
        alert("กรุณาตรวจสอบข้อมูลให้ถูกต้อง !");
        setSpine(false);
    }else{
        alert("เพิ่มข้อมูลเสร็จสิ้น");
        // router.push("/buyRice");
        window.document.getElementById("addBuyRiceReset").click();
        // router.push("/buyRice");
        setSpine(false);
    }
  }

  const onChangeRice = (val) => {
    const rice = (props['riceType']['data'].find((row)=>(row['id'] === val)));
    setRiceSelect(rice);
    setRicePrice(rice['price']);
  }

  const onChangeAddress = (val) => {
    const places = (props['places']['data'].find((row)=>(row['id'] === val)));
    setPlacesSelect(places);
  }

  const onStartChange = value => {
    // console.log("value -> ",value);
    setDate(value);
  };

  return (
    <div>
      <Head>
        <title>สหบูรณ์ข้าวไทย - เพิ่ม รับเข้าข้าวเปลือก</title>
      </Head>
      <div>
        <MainMenu callback={menuCallback} >
          <Spin  spinning={spin} tip="กำลังโหลด..." >
            <Row justify="center" style={{ marginTop: "1rem" }}>
              <Col span={24}>
                <h2>เพิ่ม รับเข้าข้าวเปลือก</h2>
              </Col>
            </Row>
            <Row justify="center" gutter={16} style={{ marginTop: "1rem" }}>
              <div className="card-form">
                  <Form form={form} name="buyRice" onFinish={onSave} layout={'horizontal'} 
                  initialValues={{
                      lot: undefined,
                      name: riceSelect['id'],
                      curentDate: date,
                      // ricePrice: ricePrice,
                      riceWeight: 0
                  }}>

                    <Form.Item label="Lot" name='lot' rules={[{ required: true }]} >
                      <Input style={{width: '100%', maxWidth: 400}} />
                    </Form.Item>

                    <Form.Item label="เลขบัตรชั่ง" name='billId' rules={[{ required: true }]} >
                      <Input style={{width: '100%', maxWidth: 400}} />
                    </Form.Item>

                    <Form.Item label="วันที่ออกบิล" name='curentDate' rules={[{ required: true }]} >
                      <DatePicker
                        style={{width: '100%', maxWidth: 400}}
                        locale={locale}
                        format="YYYY-MM-DD"
                        value={date}
                        placeholder="วันที่ออกบิล"
                        onChange={onStartChange}
                      />
                    </Form.Item>

                    <Form.Item label="ประเภทข้าว" name='name' rules={[{ required: true }]} >
                      <Select
                          showSearch
                          style={{ width: '100%', maxWidth: 400 }}
                          onChange={(e)=>{onChangeRice(e)}}
                          placeholder="ประเภทข้าว"
                          optionFilterProp="children"
                          filterOption={(input, option) =>
                            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                          }
                        >
                          {props.riceType.data.map((res, index)=>{
                            return <Option key={res.id} value={res.id}>{res.name}</Option>
                          })}
                      </Select>
                    </Form.Item>

                    <Form.Item label="ราคาข้าว" name='ricePrice' rules={[{ required: false }]} >
                      <Input
                        type="number"
                        placeholder="ราคาข้าว"
                        onChange={(e)=>{setRicePrice(parseFloat(e.target.value));}}
                        min="1"
                        max="5000"
                        step="0.01"
                        style={{ width: "100%", maxWidth: 400 }}
                      />
                    </Form.Item>
                    
                    <Form.Item label="สถาณที่" name='address' rules={[{ required: true }]} >
                      <Select
                        showSearch
                        style={{ width: '100%', maxWidth: 400 }}
                        placeholder="สถาณที่"
                        optionFilterProp="children"
                        onChange={(e)=>{onChangeAddress(e)}}
                        filterOption={(input, option) =>
                          option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {props.places.data.map((res, index)=>{
                          return <Option key={res.id} value={res.id}>{res.address}</Option>
                        })}
                      </Select>
                    </Form.Item>

                    <Form.Item label="ทะเบียนรถ" name='carRegistration' rules={[{ required: true }]} >
                      <Input placeholder="ทะเบียนรถ" style={{width: '100%', maxWidth: 400}} />
                    </Form.Item>

                    <Form.Item label="น้ำหนักข้าวสุทธิ" name='riceWeight' rules={[{ required: false }]} >
                      <Input
                        type="number"
                        placeholder="น้ำหนัก / กก."
                        onChange={(e)=>{setRiceWeight(parseFloat(e.target.value));}}
                        min="1"
                        max="5000"
                        step="0.01"
                        style={{ width: "100%", maxWidth: 400 }}
                      />
                    </Form.Item>

                    <Form.Item label="หมายเหตุ" name='description' rules={[{ required: false }]} >
                      <TextArea placeholder="หมายเหตุ" rows={3} style={{width: '100%', maxWidth: 400}} />
                    </Form.Item>

                    <Form.Item label="คิดเป็นเงิน" >
                      {` ${(riceWeight * ricePrice).toFixed(2)} บาท`} 
                    </Form.Item>

                    <Form.Item >
                      <Row gutter={8} justify="center">
                        <Col xs={0} sm={24}>
                          <center>
                            <Space>
                              <Button
                                style={{ margin: "0.5rem" }}
                                type="primary"
                                htmlType="submit"
                              >
                                บันทึก
                              </Button>

                              <Button
                                style={{ margin: "0.5rem" }}
                                id="addBuyRiceReset"
                                type="danger"
                                htmlType="Reset"
                                onClick={() => {
                                  router.push("/buyRice");
                                }}
                              >
                                ยกเลิก
                              </Button>
                            </Space>
                          </center>
                        </Col>

                        <Col xs={24} sm={0}>
                          <Button
                            style={{ margin: "0.2rem 0rem", width: "100%" }}
                            type="danger"
                            id="addBuyRiceReset"
                            htmlType="Reset"
                            block
                            onClick={() => {
                              router.push("/buyRice");
                            }}
                          >
                            ยกเลิก
                          </Button>

                          <Button
                            style={{ margin: "0.2rem 0rem" }}
                            type="primary"
                            htmlType="submit"
                            block
                          >
                            บันทึก
                          </Button>
                        </Col>
                      </Row>
                    </Form.Item>

                  </Form>
              </div>
            </Row>
          </Spin>
        </MainMenu>
      </div>
    </div>
  )
}

export default BuyRiceAdd;

export async function getServerSideProps(context) {

  const [riceType, places] = await Promise.all([
    fetch(`http://139.59.247.219:8000/rice-type`).then(r => r.json()),
    fetch(`http://139.59.247.219:8000/places`).then(r => r.json())
  ]);
  
  return {
    props:{
        riceType: riceType,
        places: places
    }
  }
}