import { useState } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import  { useEffect } from 'react';
import { Table } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import { getUser } from '../../lib/user.js';
import MainMenu from "../../components/layout/main";
import { 
  Button, 
  Tooltip,
  DatePicker, 
  Space, 
  Spin,
  Form,
  Row,
  Col,
  Typography } from 'antd';
import moment from 'moment';
import 'moment/locale/th';
import locale from 'antd/lib/locale/th_TH';
const { Title, Text } = Typography;

function buyRiceList(props){

    const router = useRouter();
    const [spin, setSpine] = useState(true);
    const [importation,setImportation] = useState({data:[]});
    const [dateStart,setDateStart] = useState(moment().startOf('day'));
    const [dateEnd,setDateEnd] = useState(moment().endOf('day'));
    const [endOpen,setEndOpen] = useState(false);
    const columns = [
        {
            title: 'ลำดับ',
            dataIndex: 'createdAt',
            align: 'center',
            width: 70,
            render:(text, record, index)=>{
              return (record['activeStatus'] === 'inactive'? <font color="red" size="2">{index + 1}</font> : <font size="2">{index + 1}</font> )
            }
        },
        {
            title: 'วันที่',
            dataIndex: 'currentDate',
            align: 'center',
            width: 150,
            render:(text, record, index)=>{
              return (record['activeStatus'] === 'inactive'? <font color="red" size="2">{moment(text).format('YYYY-MM-DD HH:mm:ss')}</font> : <font size="2">{moment(text).format('YYYY-MM-DD HH:mm:ss')}</font> ) 
            }
        },
        {
            title: 'Lot',
            align: 'center',
            dataIndex: 'lot',
            width: 60,
            render:(text, record, index)=>{
              return (record['activeStatus'] === 'inactive'? <font color="red" size="2">{text}</font> : <font size="2">{text}</font> )
            }
        },
        {
          title: 'เลขบัตรชั่ง',
          align: 'center',
          dataIndex: 'billId',
          width: 100,
          render:(text, record, index)=>{
            return (record['activeStatus'] === 'inactive'? <font color="red" size="2">{text}</font> : <font size="2">{text}</font> )
          }
        },
        {
          title: 'ทะเบียนรถ',
          align: 'center',
          width: 100,
          dataIndex: 'carRegistration',
          render:(text, record, index)=>{
            return (record['activeStatus'] === 'inactive'? <font color="red" size="2">{text}</font> : <font size="2">{text}</font> )
          }
        },
        {
          title: 'ชนิดข้าว',
          align: 'center',
          dataIndex: 'name',
          width: 100,
          filters: props.['riceType']['data'].map((row)=>({text:row['name'], value:row['name']})),
          onFilter: (value, record) => record.name.indexOf(value) === 0,
          sortDirections: ['descend'],
          render:(text, record, index)=>{
            return (record['activeStatus'] === 'inactive'? <font color="red" size="2">{text}</font> : <font size="2">{text}</font> )
          }
        },
        {
          title: 'น้ำหนัก/ กม.',
          align: 'center',
          dataIndex: 'riceWeight',
          width: 100,
          render:(text, record, index)=>{
            return (record['activeStatus'] === 'inactive'? <font color="red" size="2">{`${text.toFixed(2)}`}</font> : <font size="2">{`${text.toFixed(2)}`}</font> )
          }
        },
        {
          title: 'ราคา',
          align: 'center',
          dataIndex: 'ricePrice',
          width: 100,
          render:(text, record, index)=>{
            return (record['activeStatus'] === 'inactive'? <font color="red" size="2">{`${text.toFixed(2)} บาท`}</font> : <font size="2">{`${text.toFixed(2)} บาท`}</font> )
          }
        },
        {
          title: 'จำนวนเงิน',
          align: 'center',
          dataIndex: 'netRicePrice',
          width: 100,
          render:(text, record, index)=>{
            return (record['activeStatus'] === 'inactive'? <font color="red" size="2">{`${text.toFixed(2)} บาท`}</font> : <font size="2">{`${text.toFixed(2)} บาท`}</font> )
          }
        },
        {
          title: 'สถานที่เก็บ',
          align: 'center',
          dataIndex: 'address',
          width: 150,
          filters: props.['places']['data'].map((row)=>({text:row['address'], value:row['address']})),
          onFilter: (value, record) => record.address.indexOf(value) === 0,
          sortDirections: ['descend'],
          render:(text, record, index)=>{
            return (record['activeStatus'] === 'inactive'? <font color="red" size="2">{text}</font> : <font size="2">{text}</font> )
          }
        },
        {
          title: 'จัดการ',
          dataIndex: 'manage',
          align: 'center',
          width: 180,
          render:(text, record, index)=>{
            return (
              
                (record['activeStatus'] === 'inactive'? <font color="red">ยกเลิก</font> : 
                <div>
                  <Button style={{margin:'0.5rem', fontSize:12}} className="hover-warning" onClick={()=>{router.push(`/buyRice/${record['id']}`)}}>แก้ไข</Button> 
                  <Button style={{margin:'0.5rem', fontSize:12}} id="addEmployeeReset" type="danger" onClick={()=>{handleDelete(record)}} >ลบ</Button>
                </div>
                )
              
            )
          }
        }
    ];

  useEffect(()=>{
    mainFunc();
  },[])

  const mainFunc = () => {
    const user = getUser();
    if(user['profile'] === null) {
      router.push("/login");
    }else{
      init();
      setSpine(false);
    }
  }

  const init = async () => {
    const start = dateStart.format('YYYY-MM-DD HH:mm:ss');
    const end = dateEnd.format('YYYY-MM-DD HH:mm:ss');
    const response = await fetch(`http://139.59.247.219:8000/importation?sort=currentDate,ASC&s={"$and":[{"currentDate":{"$gt": "${start}"}},{"currentDate":{"$lt": "${end}"}}]}`).then(r => r.json());
    // response['data'] = response['data'].filter((row)=>(row.activeStatus === 'active'));
    if(!response.error){
      setTimeout(()=>{
        setImportation(response);
      },1100)
    }
  }

  const menuCallback = (param) => {
    console.log("param -> ",param);
  }

  const onStartChange = value => {
    setDateStart(value.startOf('day'));
  };

  const onEndChange = value => {
    setDateEnd(value.endOf('day'));
  };

  const handleStartOpenChange = open => {
    if (!open) {
      setEndOpen(true);
    }
  };

  const handleEndOpenChange = open => {
    setEndOpen(open);
  };

  const handleSearch = async () => {
      const start = dateStart.format('YYYY-MM-DD HH:mm:ss');
      const end = dateEnd.format('YYYY-MM-DD HH:mm:ss');
      const response = await fetch(`http://139.59.247.219:8000/importation?sort=currentDate,ASC&s={"$and":[{"currentDate":{"$gt": "${start}"}},{"currentDate":{"$lt": "${end}"}}]}`).then(r => r.json());
      // response['data'] = response['data'].filter((row)=>(row.activeStatus === 'active'));
      if(!response.error){
        setTimeout(()=>{
          setImportation(response);
        },1100)
      }
  };

  const handleDelete = async ({id, createdAt, lot}) => {

    let resultConfirm = confirm(`คุณกำลังจะลบข้อมูล วันที่ "${moment(createdAt).format('YYYY-MM-DD HH:mm:ss')}" , Lot "${lot}" ใช่หรือไม่ ?`);
    if(resultConfirm){

      setSpine(true);
      const response = await fetch(`http://139.59.247.219:8000/importation/${id}`,{
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({activeStatus:"inactive"})
      }).then(r => r);

      if(response.error){
          alert("เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง !");
          setSpine(false);
      }else{
          alert(`ลบข้อมูล วันที่ "${createdAt}", Lot "${lot}" เสร็จสิ้น`);
          router.push("/buyRice");
          setSpine(false);
      }

    }
  }

  return (
    <div>
      <Head>
        <title>สหบูรณ์ข้าวไทย - รับเข้าข้าวเปลือก</title>
      </Head>
      <div>
        <MainMenu callback={menuCallback} >
          <Spin  spinning={spin} tip="กำลังโหลด..." >
            <Row>
              <Col span={24}>
                <div style={{ marginTop: "1rem", marginBottom: "1rem" }}>
                  <Title level={4}>จัดการ รับเข้าข้าวเปลือก</Title>
                </div>
              </Col>
            </Row>
            <Row>
              <Col xs={0} sm={0} md={16} lg={16}>
                <Space>
                  วันที่เริ่มต้น: 
                  <DatePicker
                    style={{width:'100%', margin:'0.2rem 0'}}
                    locale={locale}
                    format="YYYY-MM-DD"
                    value={dateStart}
                    placeholder="วันที่เริ่มต้น"
                    onChange={onStartChange}
                    onOpenChange={handleStartOpenChange}
                  />
                  วันที่สิ้นสุด: 
                  <DatePicker
                    style={{width:'100%', margin:'0.2rem 0'}}
                    locale={locale}
                    format="YYYY-MM-DD"
                    value={dateEnd}
                    placeholder="วันที่สิ้นสุด"
                    onChange={onEndChange}
                    onOpenChange={handleEndOpenChange}
                  />
                  <Tooltip title="ค้นหา">
                    <Button shape="circle" style={{color:'#16c09e', borderColor:"#16c09e"}} onClick={()=>{handleSearch()}} icon={<SearchOutlined />} />
                  </Tooltip>
                </Space>
              </Col>

              <Col xs={0} md={8}>
                <a href="/buyRice/add">
                  <Button
                    style={{ float: "right" }}
                    type="primary"
                    shape="round"
                  >
                    เพิ่ม รับเข้าข้าว
                  </Button>
                </a>
              </Col>

              <Col xs={24} sm={24} md={0} lg={0}>
                <Row gutter={8}>
                  <Col xs={24} sm={24} md={0}>
                    <DatePicker
                      style={{width:'100%', margin:'0.2rem 0'}}
                      locale={locale}
                      format="YYYY-MM-DD"
                      value={dateStart}
                      placeholder="วันที่เริ่มต้น"
                      onChange={onStartChange}
                      onOpenChange={handleStartOpenChange}
                    />
                  </Col>
                  <Col xs={24} sm={24} md={0}>
                    <DatePicker
                      style={{width:'100%', margin:'0.2rem 0'}}
                      locale={locale}
                      format="YYYY-MM-DD"
                      value={dateEnd}
                      placeholder="วันที่สิ้นสุด"
                      onChange={onEndChange}
                      onOpenChange={handleEndOpenChange}
                    />
                  </Col>
                  <Col xs={24} sm={24} md={0}>
                      <Button style={{margin:'0.2rem 0'}} block type="primary" onClick={()=>{handleSearch()}} icon={<span>ค้นหา <SearchOutlined /></span>} />
                  </Col>
                </Row>
              </Col>
         
              <Col xs={24} md={0}>
                <a href="/buyRice/add">
                  <Button
                    style={{ margin:'0.2rem 0' }}
                    type="primary"
                    shape="round"
                    block
                  >
                    เพิ่ม รับเข้าข้าว
                  </Button>
                </a>
              </Col>

            </Row>
            <Row style={{marginTop:'1rem'}}>
              <Col className="gutter-row" span={24}>
                <Table 
                  size="small"
                  style={{ marginTop: "1rem", marginBottom: "1rem" }}
                  bordered 
                  responsive
                  columns={columns} 
                  dataSource={(importation['data'].map((element, index)=>{return Object.assign(element,{key:index});}))} 
                  pagination={{ defaultPageSize: 25 }} 
                  scroll={{ y: 520 }}
                />
              </Col>
            </Row>
          </Spin>
        </MainMenu>
      </div>
      
    </div>
  )
}

export default buyRiceList;

export async function getStaticProps(context) {

  const [riceType, places] = await Promise.all([
    fetch(`http://139.59.247.219:8000/rice-type`).then(r => r.json()),
    fetch(`http://139.59.247.219:8000/places`).then(r => r.json())
  ]);

  return {
    props:{
        riceType: riceType,
        places: places
    }
  }
}