import { useState } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import  { useEffect } from 'react';
import { Table, Space, Spin, Typography, Row, Col } from 'antd';
import { getUser } from '../../lib/user.js';
import MainMenu from "../../components/layout/main";
import { 
  Button,
  Form } from 'antd';
import moment from 'moment';
import 'moment/locale/th';
import locale from 'antd/lib/locale/th_TH';
const { Title, Text } = Typography;

function RiceTypeList(props){

  const router = useRouter();
  const [spin, setSpine] = useState(true);
  const [startValue,setStartValue] = useState(null);
  const [endValue,setEndValue] = useState(null);
  const [endOpen,setEndOpen] = useState(false);
  const [users, setUsers] = useState({data:[]});
  const columns = [
      {
          title: 'ชื่อชนิดข้าว',
          dataIndex: 'name',
          align: 'center',
          width: 120
      },
      {
          title: 'ราคา',
          dataIndex: 'price',
          align: 'center',
          width: 100,
          render:(text, record, index)=>{
              return (`${text} บาท`)
          }
      },
      {
        title: 'จัดการ',
        dataIndex: 'manage',
        align: 'center',
        width: 200,
        render:(text, record, index)=>{
          return (
            <Space>
              <Button style={{margin:'0.5rem'}} className="hover-warning" onClick={()=>{router.push(`/riceType/${record['id']}`)}}>แก้ไข</Button>
              <Button style={{margin:'0.5rem'}} type="danger" onClick={()=>{handleDelete(record)}} >ลบ</Button>
            </Space>
          )
        }
      }
  ];

  useEffect(()=>{
    mainFunc();
  },[props]);

  const mainFunc = () => {
    const user = getUser();
    if(user['profile'] === null) {
      router.push("/login");
    }else{
      init();
      setSpine(false);
    }
  }

  const init = async () => {
    const response = await fetch(`http://139.59.247.219:8000/rice-type`).then(r => r.json());
    if(!response.error){
      setUsers(response);
    }
  }

  const menuCallback = (param) => {
    console.log("param -> ",param);
  }

  const handleStartOpenChange = open => {
    if (!open) {
      setEndOpen(true);
    }
  };

  const handleEndOpenChange = open => {
    setEndOpen(open);
  };

  const handleDelete = async ({id, name}) => {

      let resultConfirm = confirm(`คุณกำลังจะลบ "${name}" ใช่หรือไม่ ?`);
      if(resultConfirm){

        setSpine(true);
        const response = await fetch(`http://139.59.247.219:8000/rice-type/${id}`,{
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json'
          },
          body: null
        }).then(r => r);

        if(response.error){
            alert("เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง !");
            setSpine(false);
        }else{
            alert(`ลบข้อมูล "${name}" เสร็จสิ้น`);
            router.push("/riceType");
            setSpine(false);
        }

      }
  }

  return (
    <div>
      <Head>
        <title>สหบูรณ์ข้าวไทย - จัดการชนิดข้าว</title>
      </Head>
      <div>
        <MainMenu callback={menuCallback} >
          <Spin  spinning={spin} tip="กำลังโหลด..." >
            <Row>
              <Col span={24}>
                <div style={{marginTop:'1rem', marginBottom:'1rem'}}><Title level={4}>จัดการ ชนิดข้าว</Title></div>
              </Col>
            </Row>
            <Row>
              <Col xs={0} md={8}>
                  <a href="/riceType/add">
                    <Button style={{float:'right'}} type="primary" shape="round">เพิ่ม ชนิดข้าว</Button>
                  </a>
              </Col>

              <Col xs={24} md={0}>
                <a href="/riceType/add">
                  <Button
                    style={{ margin:'0.2rem 0' }}
                    type="primary"
                    shape="round"
                    block
                  >
                    เพิ่ม ชนิดข้าว
                  </Button>
                </a>
              </Col>
            </Row>

            <br/>
              <Table
                size="small"
                responsive
                bordered 
                columns={columns} 
                dataSource={(users['data'].map((element, index)=>{return Object.assign(element,{key:index});}))} 
                pagination={{ pageSize: 25 }} scroll={{ y: 520 }} 
              />
            <br/>
          </Spin>
        </MainMenu>
      </div>
    </div>
  )
}

export default RiceTypeList;