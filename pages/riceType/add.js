import Head from "next/head";
import axios from "axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Table } from "antd";
import { getUser } from "../../lib/user.js";
import MainMenu from "../../components/layout/main";
import { Select, Space, Form, Button, InputNumber, Input, Spin, Row, Col } from "antd";
const { Option } = Select;

function buyRiceAdd(props) {
  // console.log("props => ",props);
  const router = useRouter();
  const [spin, setSpine] = useState(true);

  useEffect(() => {
    mainFunc();
  }, [props]);

  const mainFunc = () => {
    const user = getUser();
    if (user["profile"] === null) {
      router.push("/login");
    } else {
      setSpine(false);
    }
  };

  const menuCallback = (param) => {
    console.log("param -> ", param);
    router.push(`/${param}`);
  };

  const onSave = async ({ addRiceType }) => {
    setSpine(true);
    const obj = {
      name: addRiceType.name,
      price: parseFloat(addRiceType.price),
    };

    const response = await fetch(`http://139.59.247.219:8000/rice-type`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(obj),
    }).then((r) => r.json());

    if (response.error) {
      alert("กรุณาตรวจสอบข้อมูลให้ถูกต้อง !");
      setSpine(false);
    } else {
      alert("เพิ่มข้อมูลเสร็จสิ้น");
      // router.push("/riceType");
      window.document.getElementById("addRiceTypeReset").click();
      setSpine(false);
    }
  };

  return (
    <div>
      <Head>
        <title>สหบูรณ์ข้าวไทย - เพิ่มชนิดข้าว</title>
      </Head>
      <div>
        <MainMenu callback={menuCallback}>
          <Spin spinning={spin} tip="กำลังโหลด...">
            <div className="row">
              <h2>เพิ่ม ชนิดข้าว</h2>
            </div>
            <Row justify="center" style={{ marginTop: "1rem" }}>
              <div className="card-form">
                <Form
                  name="addRiceType"
                  onFinish={onSave}
                  layout={"horizontal"}
                >
                  <Form.Item
                    label="ชื่อข้าว"
                    name={["addRiceType", "name"]}
                    rules={[{ required: true }]}
                  >
                    <Input placeholder="ชื่อข้าว" style={{ width: "100%" }} />
                  </Form.Item>

                  <Form.Item
                    label="ราคาข้าว"
                    name={["addRiceType", "price"]}
                    rules={[{ required: true }]}
                  >
                    {/* <InputNumber
                      style={{
                        width: 300,
                      }}
                      min={1}
                      max={100}
                      step={1.0}
                      style={{ width: "100%" }}
                    /> */}
                    <Input
                      type="number"
                      min="1"
                      max="100"
                      style={{ width: "100%" }}
                    />
                  </Form.Item>

                  <Form.Item>
                    <Row gutter={8} justify="center">
                      <Col xs={0} sm={24}>
                        <center>
                          <Space>
                            <Button
                              style={{ margin: "0.5rem" }}
                              type="primary"
                              htmlType="submit"
                            >
                              บันทึก
                            </Button>

                            <Button
                              style={{ margin: "0.5rem" }}
                              id="addRiceTypeReset"
                              type="danger"
                              htmlType="Reset"
                              onClick={() => {
                                router.push("/riceType");
                              }}
                            >
                              ยกเลิก
                            </Button>
                          </Space>
                        </center>
                      </Col>

                      <Col xs={24} sm={0}>
                        <Button
                          style={{ margin: "0.2rem 0rem", width: "100%" }}
                          type="danger"
                          id="addRiceTypeReset"
                          htmlType="Reset"
                          block
                          onClick={() => {
                            router.push("/riceType");
                          }}
                        >
                          ยกเลิก
                        </Button>

                        <Button
                          style={{ margin: "0.2rem 0rem" }}
                          type="primary"
                          htmlType="submit"
                          block
                        >
                          บันทึก
                        </Button>
                      </Col>
                    </Row>
                        
                  </Form.Item>
                </Form>
              </div>
            </Row>
          </Spin>
        </MainMenu>
      </div>
    </div>
  );
}

export default buyRiceAdd;
