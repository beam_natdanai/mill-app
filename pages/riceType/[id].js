import Head from "next/head";
import axios from "axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Table } from "antd";
import { getUser } from "../../lib/user.js";
import MainMenu from "../../components/layout/main";
import { Select, Space, Form, Button, InputNumber, Input, Spin, Row, Col } from "antd";
const { Option } = Select;

function EmployeeUpdate(props) {
  const router = useRouter();
  const [spin, setSpine] = useState(true);

  useEffect(() => {
    mainFunc();
  }, [props]);

  const mainFunc = () => {
    const user = getUser();
    if (user["profile"] === null) {
      router.push("/login");
    } else {
      setSpine(false);
    }
  };

  const menuCallback = (param) => {
    console.log("param -> ", param);
    router.push(`/${param}`);
  };

  const onSave = async ({ name, price }) => {
    setSpine(true);

    const obj = {
      name: name,
      price: parseFloat(price),
    };

    const delPropsUndefined = Object.entries(obj).filter(
      (row) => row[1] !== undefined
    );

    const response = await fetch(
      `http://139.59.247.219:8000/rice-type/${props["rice"]["id"]}`,
      {
        method: "PATCH",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(Object.fromEntries(delPropsUndefined)),
      }
    ).then((r) => r.json());
    if (response.error) {
      alert("กรุณาตรวจสอบข้อมูลให้ถูกต้อง !");
      setSpine(false);
    } else {
      alert("แก้ไขข้อมูลเสร็จสิ้น");
      router.push("/riceType");
      window.document.getElementById("updateRiceTypeReset").click();
      setSpine(false);
    }
  };

  return (
    <div>
      <Head>
        <title>สหบูรณ์ข้าวไทย - แก้ไขชนิดข้าว</title>
      </Head>
      <div>
        <MainMenu callback={menuCallback}>
          <Spin spinning={spin} tip="กำลังโหลด...">
            <div className="row">
              <h2>แก้ไข ชนิดข้าว</h2>
            </div>
            <Row justify="center" style={{ marginTop: "1rem" }}>
              <div className="card-form">
                <Form
                  name="updateRiceType"
                  onFinish={onSave}
                  layout={"horizontal"}
                  initialValues={{
                    name: props["rice"]["name"],
                    price: props["rice"]["price"],
                  }}
                >
                  <Form.Item
                    label="ชื่อข้าว"
                    name={"name"}
                    rules={[{ required: true }]}
                  >
                    <Input placeholder="ชื่อข้าว" style={{ width: "100%", maxWidth: 400 }} />
                  </Form.Item>

                  <Form.Item
                    label="ราคาข้าว"
                    name="price"
                    rules={[{ required: true }]}
                  >
                    <Input
                      type="number"
                      min="1"
                      max="100"
                      step="0.50"
                      style={{ width: "100%", maxWidth: 400 }}
                    />
                  </Form.Item>

                  <Form.Item>
                    <Row gutter={8} justify="center">
                      <Col xs={0} sm={24}>
                        <center>
                          <Space>
                            <Button
                              style={{ margin: "0.5rem" }}
                              type="primary"
                              htmlType="submit"
                            >
                              บันทึก
                            </Button>

                            <Button
                              style={{ margin: "0.5rem" }}
                              id="updateRiceTypeReset"
                              type="danger"
                              htmlType="Reset"
                              onClick={() => {
                                router.push("/riceType");
                              }}
                            >
                              ยกเลิก
                            </Button>
                          </Space>
                        </center>
                      </Col>

                      <Col xs={24} sm={0}>
                        <Button
                          style={{ margin: "0.2rem 0rem", width: "100%" }}
                          type="danger"
                          id="updateRiceTypeReset"
                          htmlType="Reset"
                          block
                          onClick={() => {
                            router.push("/riceType");
                          }}
                        >
                          ยกเลิก
                        </Button>

                        <Button
                          style={{ margin: "0.2rem 0rem" }}
                          type="primary"
                          htmlType="submit"
                          block
                        >
                          บันทึก
                        </Button>
                      </Col>
                    </Row>
                  </Form.Item>
                </Form>
              </div>
            </Row>
          </Spin>
        </MainMenu>
      </div>
    </div>
  );
}

export default EmployeeUpdate;

export async function getServerSideProps(context) {
  const [rice] = await Promise.all([
    fetch(
      `http://139.59.247.219:8000/rice-type/${context["params"]["id"]}`
    ).then((r) => r.json()),
  ]);

  return {
    props: {
      rice: rice,
    },
  };
}
