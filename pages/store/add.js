import Head from "next/head";
import axios from "axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Table } from "antd";
import { getUser } from "../../lib/user.js";
import MainMenu from "../../components/layout/main";
import { 
  Select,
  Space,
  Form,
  Button,
  InputNumber,
  Input,
  Spin,
  Divider,
  DatePicker
} from "antd";
import moment from 'moment';
import 'moment/locale/th';
import locale from 'antd/lib/locale/th_TH';

const { Option } = Select;

function storeAdd(props) {
  const router = useRouter();
  const [spin, setSpine] = useState(true);

  useEffect(() => {
    mainFunc();
  }, [props]);

  const mainFunc = () => {
    const user = getUser();
    if (user["profile"] === null) {
      router.push("/login");
    } else {
      setSpine(false);
    }
  };

  const menuCallback = (param) => {
    console.log("param -> ", param);
    router.push(`/${param}`);
  };

  const onSave = async ({ addRiceType }) => {
    setSpine(true);
    const obj = {
      name: addRiceType.name,
      price: addRiceType.price,
    };

    const response = await fetch(`http://139.59.247.219:8000/rice-type`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(obj),
    }).then((r) => r.json());

    if (response.error) {
      alert("กรุณาตรวจสอบข้อมูลให้ถูกต้อง !");
      setSpine(false);
    } else {
      alert("เพิ่มข้อมูลเสร็จสิ้น");
      // router.push("/riceType");
      window.document.getElementById("addRiceTypeReset").click();
      setSpine(false);
    }
  };

  return (
    <div>
      <Head>
        <title>สหบูรณ์ข้าวไทย - สรุปสินค้าคงคลัง</title>
      </Head>
      <div>
        <MainMenu callback={menuCallback}>
          <Spin spinning={spin} tip="กำลังโหลด...">
            <div className="row">
              <h2>สรุปสินค้าคงคลัง</h2>
            </div>
            <div style={{ marginTop: "1rem" }} className="row">
              <div className="col-12">
                <Form
                  name="addRiceType"
                  onFinish={onSave}
                  layout={"horizontal"}
                >

                  <Form.Item label="วันที่สรุปยอด" name='currentDate' rules={[{ required: true }]} >
                    <DatePicker
                      style={{width: 300}}
                      locale={locale}
                      format="YYYY-MM-DD"
                      value={moment()}
                      placeholder="วันที่สรุปยอด"
                    />
                  </Form.Item>

                  <Divider orientation="left" plain><font color="#616161" size="2">ยอดข้าวเปลือก</font></Divider>
                  <Form.Item
                    label="รับเข้า ข้าวเปลือก"
                    name={["addRiceType", "name"]}
                    rules={[{ required: true }]}
                  >
                    <Input
                        type="number"
                        min="1"
                        max="5000"
                        placeholder="ยอดรับเข้าข้าว เปลือก, 2000"
                        style={{ width: '100%' }}
                    />
                  </Form.Item>

                  <Form.Item
                    label="จำหน่าย ข้าวเปลือก"
                    name={["addRiceType", "price"]}
                    rules={[{ required: true }]}
                  >
                    <Input
                        type="number"
                        min="1"
                        max="5000"
                        placeholder="ยอดจำหน่ายข้าว เปลือก, 1000"
                        style={{ width: '100%' }}
                    />
                  </Form.Item>

                  <Divider orientation="left" plain><font color="#616161" size="2">ยอดข้าวสาร</font></Divider>

                  <Form.Item
                    label="รับเข้า ข้าวสาร"
                    name={["addRiceType", "name"]}
                    rules={[{ required: true }]}
                  >
                    <Input
                        type="number"
                        min="1"
                        max="5000"
                        placeholder="ยอดรับเข้าข้าว สาร, 2000"
                        style={{ width: '100%' }}
                    />
                  </Form.Item>

                  <Form.Item
                    label="จำหน่าย ข้าวสาร"
                    name={["addRiceType", "price"]}
                    rules={[{ required: true }]}
                  >
                    <Input
                        type="number"
                        min="1"
                        max="5000"
                        placeholder="ยอดจำหน่ายข้าว สาร, 1000"
                        style={{ width: '100%' }}
                    />
                  </Form.Item>

                  <Form.Item>
                    <Button
                      style={{ margin: "0.5rem" }}
                      type="primary"
                      htmlType="submit"
                    >
                      บันทึก
                    </Button>

                    <Button
                      style={{ margin: "0.5rem" }}
                      id="addRiceTypeReset"
                      type="danger"
                      htmlType="Reset"
                      onClick={() => {
                        router.push("/store");
                      }}
                    >
                      ยกเลิก
                    </Button>
                  </Form.Item>
                </Form>
              </div>
            </div>
          </Spin>
        </MainMenu>
      </div>
    </div>
  );
}

export default storeAdd;
