import { useState } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import  { useEffect } from 'react';
import { Table, Space, Spin } from 'antd';
import { getUser } from '../../lib/user.js';
import { arrSumKey } from "../../lib/function.js";
import MainMenu from "../../components/layout/main";
import { 
  Button,
  Form, 
  Row, 
  Col, 
  Divider,
  DatePicker,
  Tooltip,
  Typography } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import moment from 'moment';
import 'moment/locale/th';
import locale from 'antd/lib/locale/th_TH';
const { Title, Text } = Typography;

function RiceTypeList(props){
    
  const router = useRouter();
  const [spin, setSpine] = useState(true);
  const [startValue,setStartValue] = useState(null);
  const [endValue,setEndValue] = useState(null);
  const [endOpen,setEndOpen] = useState(false);
  const [dateStart,setDateStart] = useState(moment().startOf('day'));
  const [dateEnd,setDateEnd] = useState(moment().endOf('day'));
  const [dataTable, setDataTable] = useState({data:[]});
  const [dataTableRice, setDataTableRice] = useState({data:[]});
  const [paddySum, setPaddySum] = useState(0);
  const [riceSum, setRiceSum] = useState(0);

  const millType = {
    start: "ข้าวต้น",
    mid: "ข้าวท่อน",
    final1: "ข้าวปลาย 1",
    final2: "ข้าวปลาย 2",
    machine: "ข้าวหลังเครื่อง"
  }

  const columns = [
    {
      title: 'ประเภทข้าว',
      dataIndex: 'riceType',
      align: 'center',
      width:100,
      render: (text, record, index) => {
        return (record["riceType"] === null ? '-' : record["riceType"]["name"]);
      },
    },
    {
        title: 'น้ำหนัก / กก.',
        dataIndex: 'weight',
        align: 'center',
        width: 100,
        render: (text, record, index) => {
          return record.balance;
        },
    }
  ];

  // const columnsRiceOld = [
  //   {
  //     title: 'ประเภทข้าว',
  //     dataIndex: 'millType',
  //     align: 'center',
  //     width: 120,
  //     children:[
  //       {
  //         title: 'ประเภท',
  //         align: 'center',
  //         dataIndex: 'millType',
  //         key: 'millType',
  //         width: 60,
  //         render:(text, record, index)=>{
  //           if((index % 2) === 0 ){
  //             return {
  //               children:text,
  //               props:{
  //                 rowSpan:2
  //               }
  //             };
  //           } else {
  //             return {
  //               children:text,
  //               props:{
  //                 rowSpan:0
  //               }
  //             };
  //           }
  //         }
  //       },
  //       {
  //         title: 'น้ำหนัก / กก.',
  //         dataIndex: 'weight',
  //         align: 'center',
  //         key: 'weight',
  //         width: 60,
  //         render:(text, record, index)=>{
  //           if((index % 2) === 0 ){
  //             return {
  //               children:text,
  //               props:{
  //                 rowSpan:2
  //               }
  //             };
  //           } else {
  //             return {
  //               children:text,
  //               props:{
  //                 rowSpan:0
  //               }
  //             };
  //           }
  //         }
  //       }
  //     ],

  //   },
  //   {
  //     title: 'บรรจุภัณฑ์',
  //     dataIndex: 'riceType',
  //     align: 'center',
  //     width:180,
  //     children:[
  //       {
  //         title: 'ชื่อ บรรจุภัณฑ์',
  //         align: 'center',
  //         dataIndex: 'packageTypeName',
  //         key: 'packageTypeName',
  //         width: 60,
  //         render:(text, record, index)=>{
  //           return text;
  //         }
  //       },
  //       {
  //         title: 'น้ำหนัก / กก.',
  //         dataIndex: 'weightPackage',
  //         align: 'center',
  //         key: 'weight',
  //         width: 60
  //       },
  //       {
  //         title: 'จำนวน ถุง',
  //         dataIndex: 'weightPackage',
  //         align: 'center',
  //         key: 'package',
  //         width: 60,
  //         render:(text, record, index)=>{

  //           if(record['packageTypeName'] === "ถุง 45"){
  //             return ((parseFloat(text) / 45).toFixed(2))
  //           } else {
  //             return ((parseFloat(text) / 100).toFixed(2))
  //           }
  //         }
  //       }
  //     ]
  //   },
  // ];

  const columnsRice = [
    {
      title: 'ประเภทข้าว',
      dataIndex: 'millType',
      align: 'center',
      width:100,
      render: (text, record, index) => {
        return millType[text]
      },
    },
    {
        title: 'น้ำหนัก / กก.',
        dataIndex: 'balance',
        align: 'center',
        width: 100,
        render: (text, record, index) => {
          return record.balance
        },
    }
  ];

  useEffect(()=>{
    mainFunc();
  },[]);

  const mainFunc = () => {
    const user = getUser();
    if(user['profile'] === null) {
      router.push("/login");
    }else{
      init();
      setSpine(false);
    }
  }



  const init = async () => {

    const [accountStore, accountStoreRices] = await Promise.all([
      fetch(`http://139.59.247.219:8000/account-store?filter=currentDate||$gte||${dateStart.startOf('day').format("YYYY-MM-DD HH:mm:ss")}&filter=currentDate||$lte||${dateStart.endOf('day').format("YYYY-MM-DD HH:mm:ss")}&sort=currentDate,ASC`).then((r) => r.json()),
      fetch(`http://139.59.247.219:8000/account-store-rices?filter=currentDate||$gte||${dateStart.startOf('day').format("YYYY-MM-DD HH:mm:ss")}&filter=currentDate||$lte||${dateStart.endOf('day').format("YYYY-MM-DD HH:mm:ss")}&sort=currentDate,ASC`).then((r) => r.json()),
    ]);
    if(!accountStore.error){
      setDataTable(accountStore);
      setDataTableRice(accountStoreRices);
    }
  }

  const menuCallback = (param) => {
    console.log("param -> ",param);
  }

  const handleSearch = async () => {
    const [accountStore, accountStoreRices] = await Promise.all([
      fetch(`http://139.59.247.219:8000/account-store?filter=currentDate||$gte||${dateStart.startOf('day').format("YYYY-MM-DD HH:mm:ss")}&filter=currentDate||$lte||${dateStart.endOf('day').format("YYYY-MM-DD HH:mm:ss")}&sort=currentDate,ASC`).then((r) => r.json()),
      fetch(`http://139.59.247.219:8000/account-store-rices?filter=currentDate||$gte||${dateStart.startOf('day').format("YYYY-MM-DD HH:mm:ss")}&filter=currentDate||$lte||${dateStart.endOf('day').format("YYYY-MM-DD HH:mm:ss")}&sort=currentDate,ASC`).then((r) => r.json()),
    ]);
    if(!accountStore.error){
      setDataTable(accountStore);
      setDataTableRice(accountStoreRices);
    }

  };

  const handleStartOpenChange = open => {
    if (!open) {
      setEndOpen(true);
    }
  };

  const handleEndOpenChange = open => {
    setEndOpen(open);
  };


  return (
    <div>
      <Head>
        <title>สหบูรณ์ข้าวไทย - สินค้าคงคลัง</title>
      </Head>
      <div>
        <MainMenu callback={menuCallback} >
          <Spin  spinning={spin} tip="กำลังโหลด..." >
            <Row>
                <Col span={24}>
                    <div style={{marginTop:'1rem', marginBottom:'1rem'}}><Title level={3}>สินค้าคงคลัง</Title></div>
                </Col>
            </Row>
            <Row>
                <Col xs={0} sm={0} md={24} lg={24}>
                  <Space>
                      วันที่: 
                      <DatePicker
                        locale={locale}
                        format="YYYY-MM-DD"
                        value={dateStart}
                        placeholder="วันที่"
                        onChange={(value)=>{setDateStart(value.startOf('day'))}}
                        onOpenChange={handleStartOpenChange}
                      />
                      {/* วันที่สิ้นสุด: 
                      <DatePicker
                        locale={locale}
                        format="YYYY-MM-DD"
                        value={dateEnd}
                        placeholder="วันที่สิ้นสุด"
                        onChange={(value)=>{setDateEnd(value.endOf('day'))}}
                        onOpenChange={handleEndOpenChange}
                      /> */}
                      <Tooltip title="ค้นหา">
                        <Button shape="circle" type="primary" onClick={()=>{handleSearch()}} icon={<SearchOutlined />} />
                      </Tooltip>
                  </Space>
                </Col>
                <Col xs={24} sm={24} md={0} lg={0}>
                  <Row gutter={8}>
                    <Col xs={24} sm={24} md={0}>
                      <DatePicker
                        style={{width:'100%', margin:'0.2rem 0'}}
                        locale={locale}
                        format="YYYY-MM-DD"
                        value={dateStart}
                        placeholder="วันที่เริ่มต้น"
                        onChange={(value)=>{setDateStart(value.startOf('day'))}}
                        onOpenChange={handleStartOpenChange}
                      />
                    </Col>
                    {/* <Col xs={24} sm={24} md={0}>
                      <DatePicker
                        style={{width:'100%', margin:'0.2rem 0'}}
                        locale={locale}
                        format="YYYY-MM-DD"
                        value={dateEnd}
                        placeholder="วันที่สิ้นสุด"
                        onChange={(value)=>{setDateEnd(value.endOf('day'))}}
                        onOpenChange={handleEndOpenChange}
                      />
                    </Col> */}
                    <Col xs={24} sm={24} md={0}>
                      <Button style={{margin:'0.2rem 0'}} block type="primary" onClick={()=>{handleSearch()}} icon={<span>ค้นหา <SearchOutlined /></span>} />
                    </Col>
                  </Row>
                </Col>
            </Row>
            <Row style={{marginTop:'2rem', marginBottom:'1rem'}} gutter={16}>
                <Col className="gutter-row"  span={24}>
                  <span style={{margin:'1rem 0'}}>● รายการสรุปยอด ข้าวเปลือก</span>
                </Col>
                <Col className="gutter-row" span={24}>
                    <Table 
                    bordered
                    responsive
                    size="small"
                    columns={columns} 
                    dataSource={dataTable.data.map((element, index)=>{return Object.assign(element,{key:index});})} 
                    pagination={false} 
                    summary={pageData => {

                      setPaddySum(arrSumKey(pageData, 'balance'))

                      return (
                        <>
                          <Table.Summary.Row>
                            <Table.Summary.Cell colSpan={1}><center><Text strong level={4}>รวม</Text></center></Table.Summary.Cell>
                            <Table.Summary.Cell>
                              <Text strong><center>{paddySum}</center></Text>
                            </Table.Summary.Cell>
                          </Table.Summary.Row>
                        </>
                      );
                    }}
                    />
                </Col>
            </Row>

            <Row style={{marginTop:'2rem', marginBottom:'1rem'}} gutter={16}>
                <Col className="gutter-row"  span={24}>
                  <span style={{margin:'1rem 0'}}>● รายการสรุปยอด ข้าวสาร</span>
                </Col>
                <Col className="gutter-row" span={24}>
                    <Table 
                    bordered
                    responsive
                    size="small"
                    columns={columnsRice} 
                    dataSource={ dataTableRice.data.map((element, index)=>{return Object.assign(element,{key:index});}) } 
                    pagination={{ pageSize: 25 }} scroll={{ y: 520 }} 
                    summary={pageData => {

                      setRiceSum(arrSumKey(pageData, 'balance'))

                      return (
                        <>
                          <Table.Summary.Row>
                            <Table.Summary.Cell colSpan={1}><center><Text strong level={4}>รวม</Text></center></Table.Summary.Cell>
                            <Table.Summary.Cell>
                              <Text strong><center>{riceSum}</center></Text>
                            </Table.Summary.Cell>
                          </Table.Summary.Row>
                        </>
                      );
                    }}
                    />
                </Col>
            </Row>

          </Spin>
        </MainMenu>
      </div>
    </div>
  )
}

export default RiceTypeList;

export async function getServerSideProps(context) {
  const [riceType] = await Promise.all([
    fetch(`http://139.59.247.219:8000/rice-type`).then((r) => r.json()),
  ]);

  return {
    props: {
      riceType: riceType,
    },
  };
}