import Head from "next/head";
import axios from "axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Table } from "antd";
import { getUser } from "../../lib/user.js";
import MainMenu from "../../components/layout/main";
import { Select, Space, Form, Button, InputNumber, Input, Spin, Row, Col } from "antd";
const { Option } = Select;

function buyRiceAdd(props) {
  const router = useRouter();
  const [spin, setSpine] = useState(true);

  useEffect(() => {
    mainFunc();
  }, [props]);

  const mainFunc = () => {
    const user = getUser();
    if (user["profile"] === null) {
      router.push("/login");
    } else {
      setSpine(false);
    }
  };

  const menuCallback = (param) => {
    console.log("param -> ", param);
    router.push(`/${param}`);
  };

  const onSave = async ({ addPlaces }) => {
    setSpine(true);

    const obj = {
        address: addPlaces.address
    };

    const response = await fetch(`http://139.59.247.219:8000/places`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(obj),
    }).then((r) => r.json());

    if (response.error) {
      alert("กรุณาตรวจสอบข้อมูลให้ถูกต้อง !");
      setSpine(false);
    } else {
      alert("เพิ่มข้อมูลเสร็จสิ้น");
      // router.push("/riceType");
      window.document.getElementById("addPlacesReset").click();
      setSpine(false);
    }
  };

  return (
    <div>
      <Head>
        <title>สหบูรณ์ข้าวไทย - เพิ่ม สถานที่</title>
      </Head>
      <div>
        <MainMenu callback={menuCallback}>
          <Spin spinning={spin} tip="กำลังโหลด...">
            <Row>
              <Col span={24}>
                <h2>เพิ่ม สถานที่</h2>
              </Col>
            </Row>
            <Row justify="center" gutter={16} style={{ marginTop: "1rem" }}>
              <div className="card-form">
                <Col xs={24}>
                  <Form
                    name="addPlaces"
                    onFinish={onSave}
                    layout={"horizontal"}
                  >
                    <Form.Item
                      label="สถานที่เก็บ"
                      name={["addPlaces", "address"]}
                      rules={[{ required: true }]}
                    >
                      <Input placeholder="ชื่อสถานที่เก็บ" style={{ width: "100%" }} />
                    </Form.Item>

                    <Form.Item >
                      <Row gutter={8} justify="center">
                        <Col xs={0} sm={24}>
                          <center>
                            <Space>

                              <Button
                                style={{ margin: "0.5rem" }}
                                type="primary"
                                htmlType="submit"
                              >
                                บันทึก
                              </Button>

                              <Button
                                style={{ margin: "0.5rem" }}
                                id="addPlacesReset"
                                type="danger"
                                htmlType="Reset"
                                onClick={() => {
                                  router.push("/place");
                                }}
                              >
                                ยกเลิก
                              </Button>

                            </Space>
                          </center>
                        </Col>

                        <Col xs={24} sm={0}>
                          <Button
                            style={{ margin: "0.2rem 0rem", width: "100%" }}
                            type="danger"
                            id="addChapterReset"
                            htmlType="Reset"
                            block
                            onClick={() => {
                              router.push("/place");
                            }}
                          >
                            ยกเลิก
                          </Button>

                          <Button
                            style={{ margin: "0.2rem 0rem" }}
                            type="primary"
                            htmlType="submit"
                            block
                          >
                            บันทึก
                          </Button>
                        </Col>
                      </Row>
                    </Form.Item>

                  </Form>
                </Col>
              </div>
            </Row>

          </Spin>
        </MainMenu>
      </div>
    </div>
  );
}

export default buyRiceAdd;
