import Head from "next/head";
import axios from "axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Table } from "antd";
import { getUser } from "../../lib/user.js";
import MainMenu from "../../components/layout/main";
import { Select, Space, Form, Button, InputNumber, Input, Spin, Row, Col } from "antd";
const { Option } = Select;

function EmployeeUpdate(props) {
  const router = useRouter();
  const [spin, setSpine] = useState(true);

  useEffect(() => {
    mainFunc();
  }, [props]);

  const mainFunc = () => {
    const user = getUser();
    if (user["profile"] === null) {
      router.push("/login");
    } else {
      setSpine(false);
    }
  };

  const menuCallback = (param) => {
    console.log("param -> ", param);
    router.push(`/${param}`);
  };

  const onSave = async ({ address }) => {
    setSpine(true);

    const obj = {
        address: address
    };

    const delPropsUndefined = Object.entries(obj).filter(
      (row) => row[1] !== undefined
    );

    const response = await fetch(
      `http://139.59.247.219:8000/places/${props["places"]["id"]}`,
      {
        method: "PATCH",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(Object.fromEntries(delPropsUndefined)),
      }
    ).then((r) => r.json());

    if (response.error) {
      alert("กรุณาตรวจสอบข้อมูลให้ถูกต้อง !");
      setSpine(false);
    } else {
      alert("แก้ไขข้อมูลเสร็จสิ้น");
      router.push("/place");
      window.document.getElementById("updatePlaceReset").click();
      setSpine(false);
    }
  };

  return (
    <div>
      <Head>
        <title>สหบูรณ์ข้าวไทย - แก้ไข สถานที่</title>
      </Head>
      <div>
        <MainMenu callback={menuCallback}>
          <Spin spinning={spin} tip="กำลังโหลด...">
            <Row>
              <Col span={24}>
                <h2>แก้ไข สถานที่</h2>
              </Col>
            </Row>

            <Row justify="center" style={{ marginTop: "1rem" }}>
              <div className="card-form">
                <Col xs={24}>
                  <Form
                    name="updateRiceType"
                    onFinish={onSave}
                    layout={"horizontal"}
                    initialValues={{
                      address: props["places"]["address"]
                    }}
                  >
                    <Form.Item
                      label="สถานที่เก็บ"
                      name="address"
                      rules={[{ required: true }]}
                    >
                      <Input placeholder="ชื่อสถานที่เก็บ" style={{ width: "100%" }} />
                    </Form.Item>

                    <Form.Item >
                      <Row gutter={8} justify="center">
                        <Col xs={0} sm={24}>
                          <center>
                            <Space>

                              <Button
                                style={{ margin: "0.5rem" }}
                                type="primary"
                                htmlType="submit"
                              >
                                บันทึก
                              </Button>

                              <Button
                                style={{ margin: "0.5rem" }}
                                id="addPlacesReset"
                                type="danger"
                                htmlType="Reset"
                                onClick={() => {
                                  router.push("/place");
                                }}
                              >
                                ยกเลิก
                              </Button>

                            </Space>
                          </center>
                        </Col>

                        <Col xs={24} sm={0}>
                          <Button
                            style={{ margin: "0.2rem 0rem", width: "100%" }}
                            type="danger"
                            id="updatePlaceReset"
                            htmlType="Reset"
                            block
                            onClick={() => {
                              router.push("/place");
                            }}
                          >
                            ยกเลิก
                          </Button>

                          <Button
                            style={{ margin: "0.2rem 0rem" }}
                            type="primary"
                            htmlType="submit"
                            block
                          >
                            บันทึก
                          </Button>
                        </Col>
                      </Row>
                    </Form.Item>

                  </Form>
                </Col>
              </div>
            </Row>

          </Spin>
        </MainMenu>
      </div>
    </div>
  );
}

export default EmployeeUpdate;

export async function getServerSideProps(context) {
  const [places] = await Promise.all([
    fetch(
      `http://139.59.247.219:8000/places/${context["params"]["id"]}`
    ).then((r) => r.json()),
  ]);

  return {
    props: {
        places: places,
    },
  };
}
