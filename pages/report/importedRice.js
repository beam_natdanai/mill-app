import { useState } from 'react';
import dynamic from "next/dynamic";
import Head from 'next/head';
import { useRouter } from 'next/router';
import  { useEffect } from 'react';
import { Table, Radio } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import { getUser } from '../../lib/user.js';
import { groupByAndValue, sumForGroup, arrSum } from '../../lib/function.js';
import MainMenu from "../../components/layout/main";
const Chart = dynamic(
  () => {
    return import('react-apexcharts');
  },
  { ssr: false }
);
import {
  Button, 
  Tooltip,
  DatePicker, 
  Space, 
  Spin,
  Row,
  Col,
  Typography,
  Form } from 'antd';
import moment from 'moment';
import 'moment/locale/th';
import locale from 'antd/lib/locale/th_TH';
const { Title, Text } = Typography;

function buyRiceList(props){

    const router = useRouter();
    const [spin, setSpine] = useState(true);
    const [importation,setImportation] = useState({data:[]});
    const [dateStart,setDateStart] = useState(moment().startOf('day'));
    const [dateEnd,setDateEnd] = useState(moment().endOf('day'));
    const [endOpen,setEndOpen] = useState(false);
    const [pieChart,setPieChart] = useState([]);
    const [filterChart,setFilterChart] = useState('total');
    const [seriesColumn,setSeriesColumn] = useState([]);

    const columns = [
        {
          title: 'ชนิดข้าว',
          align: 'center',
          dataIndex: 'name',
          width: 110
        },
        {
          title: 'ราคาเฉลี่ย',
          align: 'center',
          width: 140,
          dataIndex: 'ricePriceTotal',
          render:(text, record, index)=>{
            return (`${ (parseFloat(record['ricePriceTotal'] / record['ricePriceDivide'])).toFixed(2) } บาท`);
          }
        },
        {
          title: 'น้ำหนัก/กก.',
          align: 'center',
          width: 140,
          dataIndex: 'riceWeightTotal',
          render:(text, record, index)=>{
            return (`${text.toFixed(2)} กิโลกรัม`);
          }
        },
        {
          title: 'จำนวนเงิน',
          align: 'center',
          width: 140,
          dataIndex: 'netRicePriceTotal',
          render:(text, record, index)=>{
            return (`${text.toFixed(2)} บาท`);
          }
        }

    ];

  useEffect(()=>{
    mainFunc();
  },[props])

  const mainFunc = () => {
    const user = getUser();
    if(user['profile'] === null) {
      router.push("/login");
    }else{
      init();
      setSpine(false);
    }
  }

  const groupMonthSummary = (_array, _filterChart) => {

    const m1 = _array.filter((element) => (element.month === '01'));
    const m2 = _array.filter((element) => (element.month === '02'));
    const m3 = _array.filter((element) => (element.month === '03'));
    const m4 = _array.filter((element) => (element.month === '04'));
    const m5 = _array.filter((element) => (element.month === '05'));
    const m6 = _array.filter((element) => (element.month === '06'));
    const m7 = _array.filter((element) => (element.month === '07'));
    const m8 = _array.filter((element) => (element.month === '08'));
    const m9 = _array.filter((element) => (element.month === '09'));
    const m10 = _array.filter((element) => (element.month === '10'));
    const m11 = _array.filter((element) => (element.month === '11'));
    const m12 = _array.filter((element) => (element.month === '12'));

    return [
      arrSum(m1.map((row)=>(_filterChart === 'total' ? row['netRicePrice'] : row['riceWeight'] ))).toFixed(2),
      arrSum(m2.map((row)=>(_filterChart === 'total' ? row['netRicePrice'] : row['riceWeight'] ))).toFixed(2),
      arrSum(m3.map((row)=>(_filterChart === 'total' ? row['netRicePrice'] : row['riceWeight'] ))).toFixed(2),
      arrSum(m4.map((row)=>(_filterChart === 'total' ? row['netRicePrice'] : row['riceWeight'] ))).toFixed(2),
      arrSum(m5.map((row)=>(_filterChart === 'total' ? row['netRicePrice'] : row['riceWeight'] ))).toFixed(2),
      arrSum(m6.map((row)=>(_filterChart === 'total' ? row['netRicePrice'] : row['riceWeight'] ))).toFixed(2),
      arrSum(m7.map((row)=>(_filterChart === 'total' ? row['netRicePrice'] : row['riceWeight'] ))).toFixed(2),
      arrSum(m8.map((row)=>(_filterChart === 'total' ? row['netRicePrice'] : row['riceWeight'] ))).toFixed(2),
      arrSum(m9.map((row)=>(_filterChart === 'total' ? row['netRicePrice'] : row['riceWeight'] ))).toFixed(2),
      arrSum(m10.map((row)=>(_filterChart === 'total' ? row['netRicePrice'] : row['riceWeight'] ))).toFixed(2),
      arrSum(m11.map((row)=>(_filterChart === 'total' ? row['netRicePrice'] : row['riceWeight'] ))).toFixed(2),
      arrSum(m12.map((row)=>(_filterChart === 'total' ? row['netRicePrice'] : row['riceWeight'] ))).toFixed(2),
    ];

  }

  const init = async () => {
    const groupByName = groupByAndValue('name');
    const start = dateStart.format('YYYY-MM-DD HH:mm:ss');
    const end = dateEnd.format('YYYY-MM-DD HH:mm:ss');
    const startYear = moment().format('YYYY-MM-DD HH:mm:ss');
    const curentYear = dateEnd.format('YYYY-MM-DD HH:mm:ss');
    const response = await fetch(`http://139.59.247.219:8000/importation?s={"$and":[{"currentDate":{"$gt": "${start}"}},{"currentDate":{"$lt": "${end}"}}]}`).then(r => r.json());
    const responseColumnChart = await fetch(`http://139.59.247.219:8000/importation/?filter=currentDate||$gt||${start}&filter=currentDate||$lt||${end}`).then(r => r.json());
    if(!response.error){
      const dataGroupBy = groupByName(response['data'].filter((row)=>(row.activeStatus === 'active')) );
      setPieChart(sumForGroup(dataGroupBy));
      setImportation({data:sumForGroup(dataGroupBy)});
    }
    if(!responseColumnChart.error){
      const dataColumnGroupBy = groupByName(responseColumnChart['data'].filter((row)=>(row.activeStatus === 'active')) );
      const dataSeies = Object.entries((dataColumnGroupBy)).map((row)=>{
        return {
          name:row[0],
          data: groupMonthSummary(row[1],filterChart)
        }
      });
      setSeriesColumn(dataSeies);
    }

  }

  const handleSearch = async () => {

    const groupByName = groupByAndValue('name');
    const start = dateStart.format('YYYY-MM-DD HH:mm:ss');
    const end = dateEnd.format('YYYY-MM-DD HH:mm:ss');

    const response = await fetch(`http://139.59.247.219:8000/importation?fields=name,ricePrice,netRicePrice,riceWeight,activeStatus,currentDate&s={"$and":[{"currentDate":{"$gt":"${start}"}},{"currentDate":{"$lt":"${end}"}}]}`).then(r => r.json());
    const responseColumnChart = await fetch(`http://139.59.247.219:8000/importation/?filter=currentDate||$gt||${start}&filter=currentDate||$lt||${end}`).then(r => r.json());
    if(!response.error){
      const dataGroupBy = groupByName(response['data'].filter((row)=>(row.activeStatus === 'active')) );
      setPieChart(sumForGroup(dataGroupBy));
      setImportation({data:sumForGroup(dataGroupBy)});
    }
    if(!responseColumnChart.error){
      const dataColumnGroupBy = groupByName(responseColumnChart['data'].filter((row)=>(row.activeStatus === 'active')) );
      const dataSeies = Object.entries((dataColumnGroupBy)).map((row)=>{
        return {
          name:row[0],
          data: groupMonthSummary(row[1],filterChart)
        }
      });

      setSeriesColumn(dataSeies);

    }

  };

  const menuCallback = (param) => {
    // console.log("param -> ",param);
  }

  const onStartChange = value => {
    setDateStart(value.startOf('day'));
  };

  const onEndChange = value => {
    setDateEnd(value.endOf('day'));
  };

  const handleStartOpenChange = open => {
    if (!open) {
      setEndOpen(true);
    }
  };

  const handleEndOpenChange = open => {
    setEndOpen(open);
  };

  const setDataChart = (_data) => {
      return Object.entries(_data).map((row, index)=>({
        name:row[0],
        data:row[1]
      }));
  }

  const filterChartOnChange = (_value) => {
    setFilterChart(_value['target']['value']);
    handleSearch();// for column chart
  }

  return (
    <div>
      <Head>
        <title>สหบูรณ์ข้าวไทย - สรุปยอด รับเข้าข้าว</title>
      </Head>
      <div>
        <MainMenu callback={menuCallback} >
          <Spin  spinning={spin} tip="กำลังโหลด..." >
            <Row>
                <Col span={24}>
                  <div style={{marginTop:'1rem', marginBottom:'1rem'}}><Title level={4}>สรุปยอด รับเข้าข้าว</Title></div>
                </Col>
            </Row>
            <Row>
              <Col xs={0} sm={0} md={24} lg={24}>
                <Space>
                  วันที่เริ่มต้น: 
                  <DatePicker
                    locale={locale}
                    format="YYYY-MM-DD"
                    value={dateStart}
                    placeholder="วันที่เริ่มต้น"
                    onChange={onStartChange}
                    onOpenChange={handleStartOpenChange}
                  />
                  วันที่สิ้นสุด: 
                  <DatePicker
                    locale={locale}
                    format="YYYY-MM-DD"
                    value={dateEnd}
                    placeholder="วันที่สิ้นสุด"
                    onChange={onEndChange}
                    onOpenChange={handleEndOpenChange}
                  />
                  <Tooltip title="ค้นหา">
                    <Button shape="circle" style={{color:'#16c09e', borderColor:"#16c09e"}} icon={<SearchOutlined />} onClick={()=>{handleSearch()}} />
                  </Tooltip>
                </Space>
              </Col>

              <Col xs={24} sm={24} md={0} lg={0}>
                <Row gutter={8}>
                  <Col xs={24} sm={24} md={0}>
                    <DatePicker
                      style={{width:'100%', margin:'0.2rem 0'}}
                      locale={locale}
                      format="YYYY-MM-DD"
                      value={dateStart}
                      placeholder="วันที่เริ่มต้น"
                      onChange={onStartChange}
                      onOpenChange={handleStartOpenChange}
                    />
                  </Col>
                  <Col xs={24} sm={24} md={0}>
                    <DatePicker
                      style={{width:'100%', margin:'0.2rem 0'}}
                      locale={locale}
                      format="YYYY-MM-DD"
                      value={dateEnd}
                      placeholder="วันที่สิ้นสุด"
                      onChange={onEndChange}
                      onOpenChange={handleEndOpenChange}
                    />
                  </Col>
                  <Col xs={24} sm={24} md={0}>
                    <Button style={{margin:'0.2rem 0'}} block type="primary" onClick={()=>{handleSearch()}} icon={<span>ค้นหา <SearchOutlined /></span>} />
                  </Col>
                </Row>
              </Col>
            </Row>
            {(pieChart.length > 0 && 
              // <div style={{marginTop: "2rem",marginBottom: "2rem"}}>

                <Row style={{marginTop: "1rem",marginBottom: "1rem"}}>
                  <Col xs={24}>
                    <strong>แสดงกราฟ : </strong>
                    <Radio.Group onChange={filterChartOnChange} value={filterChart}>
                      <Radio value={"total"}>ราคา</Radio>
                      <Radio value={"weight"}>น้ำหนัก</Radio>
                    </Radio.Group>
                  </Col>
                  <Col xs={24} md={0}>
                    <center>
                      <Space> 
                        <Chart
                          options={{
                            chart: {
                              width: 200,
                              type: 'pie',
                            },
                            labels: pieChart.map((row)=>(row['name'])),
                            responsive: [{
                              breakpoint: 480,
                              options: {
                                chart: {
                                  width: 260
                                },
                                legend: {
                                  position: 'bottom'
                                }
                              }
                            }]
                          }}
                          series={ pieChart.map((row)=>( filterChart === 'total'? row['netRicePriceTotal'] : row['riceWeightTotal'] )) }
                          type="pie"
                          width="290"
                        />
                      </Space>
                    </center>
                  </Col>

                  <Col xs={0} md={24}>
                    <center>
                      <Space> 
                        <Chart
                          options={{
                            chart: {
                              width: 380,
                              type: 'pie',
                            },
                            labels: pieChart.map((row)=>(row['name'])),
                            responsive: [{
                              breakpoint: 420,
                              options: {
                                chart: {
                                  width: 380
                                },
                                legend: {
                                  position: 'bottom'
                                }
                              }
                            }]
                          }}
                          series={ pieChart.map((row)=>( filterChart === 'total'? row['netRicePriceTotal'] : row['riceWeightTotal'] )) }
                          type="pie"
                          width="500"
                        />
                      </Space>
                    </center>
                  </Col>

                  <Col xs={24} md={0}>
                    <center>
                      <Space>
                        <Chart
                          options={{
                            chart: {
                              width: '100%',
                              type: 'bar',
                            },
                            plotOptions: {
                              bar: {
                                horizontal: false,
                                columnWidth: '55%',
                                endingShape: 'rounded'
                              },
                            },
                            dataLabels: {
                              enabled: false
                            },
                            stroke: {
                              show: true,
                              width: 2,
                              colors: ['transparent']
                            },
                            xaxis: {
                              categories: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
                            },
                            yaxis: {
                              title: {
                                text: ""
                              }
                            },
                            fill: {
                              opacity: 1
                            },
                            tooltip: {
                              y: {
                                formatter: function (val) {
                                  return "$ " + val + " thousands"
                                }
                              }
                            }
                          }}
                          series={ seriesColumn }
                          type="bar"
                          width="100%"
                        />
                      </Space>
                    </center>
                  </Col>

                  <Col xs={0} md={24}>
                    <center>
                      <Chart
                        options={{
                          chart: {
                            width: 620,
                            type: 'bar',
                          },
                          plotOptions: {
                            bar: {
                              horizontal: false,
                              columnWidth: '55%',
                              endingShape: 'rounded'
                            },
                          },
                          dataLabels: {
                            enabled: false
                          },
                          stroke: {
                            show: true,
                            width: 2,
                            colors: ['transparent']
                          },
                          xaxis: {
                            categories: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
                          },
                          yaxis: {
                            title: {
                              text: ""
                            }
                          },
                          fill: {
                            opacity: 1
                          },
                          tooltip: {
                            y: {
                              formatter: function (val) {
                                return "$ " + val + " thousands"
                              }
                            }
                          }
                        }}
                        series={ seriesColumn }
                        type="bar"
                        width="75%"
                      />
                    </center>
                  </Col>
                </Row>
              // </div>
            )}
              
            <Row gutter={16}>
              <Col xs={24}>
                <Table 
                style={{ marginTop: "1rem", marginBottom: "1rem", width:'100%' }}
                responsive
                size="small"
                bordered 
                columns={columns} 
                dataSource={(importation['data'].map((element, index)=>{return Object.assign(element,{key:index});}))} 
                pagination={{ defaultPageSize: 25 }} 
                scroll={{ y: 520 }}
                />
              </Col>
            </Row>
            
          </Spin>
        </MainMenu>
      </div>
    </div>
  )
}

export default buyRiceList;