import 'antd/dist/antd.css';
import '../styles/globals.css';
import '../styles/components.css';
import '../styles/custom.css';
import { AppWrapper } from '../context/state';

function MyApp({ Component, pageProps }) {
  console.log("on ready");
  return (
    <AppWrapper>
      <Component {...pageProps} />
    </AppWrapper>
  );
}

export default MyApp
