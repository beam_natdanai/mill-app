import Head from "next/head";
import axios from "axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Table, DatePicker } from "antd";
import { getUser } from "../../lib/user.js";
import MainMenu from "../../components/layout/main";
import { useAppContext } from '../../context/state';
import { Select, Space, Form, Button, InputNumber, Input, Spin, Row, Col } from "antd";
import moment from 'moment';
import 'moment/locale/th';
import locale from 'antd/lib/locale/th_TH';
const { Option } = Select;
const { TextArea } = Input;

function cuttingStock(props) {
  // console.log("props => ",props);
  const router = useRouter();
  const [spin, setSpine] = useState(true);
  let _useAppContext = useAppContext();

  useEffect(() => {
    mainFunc();
  }, [props]);

  const mainFunc = () => {
    const user = getUser();
    if (user["profile"] === null) {
      router.push("/login");
    } else {
      setSpine(false);
    }
  };

  const menuCallback = (param) => {
    console.log("param -> ", param);
    router.push(`/${param}`);
  };

  const onSave = async (_value) => {

    setSpine(true);

    const obj = {
      weight: parseFloat(_value.weight),
      currentDate: moment(_value.currentDate).format("YYYY-MM-DD HH:mm:ss"),
      description: _value.description,
      cutType: "จำหน่ายสี",
      riceType: _value.riceType,
      user: _useAppContext['user']['data']['id']
    }
    // console.log("obj -> ",obj);
    const response = await fetch(`http://139.59.247.219:8000/cutting-stock`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(obj),
    }).then((r) => r.json());

    if (response.error) {
      alert("กรุณาตรวจสอบข้อมูลให้ถูกต้อง !");
      setSpine(false);
    } else {
      alert("เพิ่มข้อมูลเสร็จสิ้น");
      window.document.getElementById("addRiceReset").click();
      setSpine(false);
    }

  };

  return (
    <div>
      <Head>
        <title>สหบูรณ์ข้าวไทย - จำหน่าย ข้าวเปลือก</title>
      </Head>
      <div>
        <MainMenu callback={menuCallback}>
          <Spin spinning={spin} tip="กำลังโหลด...">
            <Row justify="center" style={{ marginTop: "1rem" }}>
              <Col span={24}>
                <h2>จำหน่าย ข้าวเปลือก</h2>
              </Col>
            </Row>
            <Row justify="center" gutter={16} style={{ marginTop: "1rem" }}>
              <div className="card-form">
                <Form
                  name="addPlaces"
                  onFinish={onSave}
                  layout={"horizontal"}
                >
                  <Form.Item label="วันที่จำหน่าย" name='currentDate' rules={[{ required: true }]} >
                    <DatePicker
                      style={{width: '100%', maxWidth: 400}}
                      locale={locale}
                      format="YYYY-MM-DD"
                      value={moment()}
                      placeholder="วันที่จำหน่าย"
                    />
                  </Form.Item>


                    <Form.Item label="ประเภทข้าว" name='riceType' rules={[{ required: true }]} >
                      <Select
                          showSearch
                          style={{ width: '100%', maxWidth: 400 }}
                          placeholder="ประเภทข้าว"
                          optionFilterProp="children"
                          filterOption={(input, option) =>
                            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                          }
                        >
                          {props.riceType.data.map((res, index)=>{
                            return <Option key={res.id} value={res.id}>{res.name}</Option>
                          })}
                      </Select>
                    </Form.Item>

                    <Form.Item
                        label="น้ำหนัก / กก."
                        name="weight"
                        rules={[{ required: true }]}
                    >
                      <Input
                        type="number"
                        placeholder="น้ำหนัก / กก."
                        min="1"
                        max="5000"
                        style={{ width: "100%", maxWidth: 400 }}
                      />
                    </Form.Item>

                    <Form.Item label="รายละเอียด" name='description' rules={[{ required: false }]} >
                      <TextArea placeholder="หมายเหตุ" rows={3} style={{width: '100%', maxWidth: 400}} />
                    </Form.Item>

                    <Form.Item>
                      <Row gutter={8} justify="center">
                        <Col xs={0} sm={24}>
                          <center>
                            <Space>
                              <Button
                                style={{ margin: "0.5rem" }}
                                type="primary"
                                htmlType="submit"
                              >
                              บันทึก
                              </Button>

                              <Button
                                style={{ margin: "0.5rem" }}
                                id="addRiceReset"
                                type="danger"
                                htmlType="Reset"
                                onClick={() => {
                                    router.push("/cuttingStock");
                                }}
                              >
                              ยกเลิก
                              </Button>
                            </Space>
                          </center>
                        </Col>

                        <Col xs={24} sm={0}>
                          <Button
                            style={{ margin: "0.2rem 0rem", width: "100%" }}
                            type="danger"
                            id="addRiceReset"
                            htmlType="Reset"
                            block
                            onClick={() => {
                              router.push("/cuttingStock");
                            }}
                          >
                            ยกเลิก
                          </Button>

                          <Button
                            style={{ margin: "0.2rem 0rem" }}
                            type="primary"
                            htmlType="submit"
                            block
                          >
                            บันทึก
                          </Button>
                        </Col>
                      </Row>
                    </Form.Item>
                </Form>
              </div>
            </Row>
          </Spin>
        </MainMenu>
      </div>
    </div>
  );
}

export default cuttingStock;

export async function getServerSideProps(context) {

    const [riceType, places] = await Promise.all([
      fetch(`http://139.59.247.219:8000/rice-type`).then(r => r.json())
    ]);
    
    return {
      props:{
          riceType: riceType
      }
    }
  }