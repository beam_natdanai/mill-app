import Head from "next/head";
import axios from "axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Table, DatePicker } from "antd";
import { getUser } from "../../lib/user.js";
import MainMenu from "../../components/layout/main";
import { useAppContext } from "../../context/state";
import { Select, Space, Form, Button, InputNumber, Input, Spin, Row, Col } from "antd";
import moment from "moment";
import "moment/locale/th";
import locale from "antd/lib/locale/th_TH";
const { Option } = Select;
const { TextArea } = Input;

function cuttingStock(props) {

  const router = useRouter();
  const [spin, setSpine] = useState(true);
  let _useAppContext = useAppContext();

  useEffect(() => {
    mainFunc();
  }, []);

  const mainFunc = () => {
    const user = getUser();
    if (user["profile"] === null) {
      router.push("/login");
    } else {
      setSpine(false);
    }
  };

  const menuCallback = (param) => {
    console.log("param -> ", param);
    router.push(`/${param}`);
  };

  const onSave = async (_value) => {
    setSpine(true);

    const obj = {
      weight: parseInt(_value.weight),
      currentDate: moment(_value.currentDate).format("YYYY-MM-DD HH:mm:ss"),
      description: _value.description,
      millType: _value.millType
    };

    const delPropsUndefined = Object.entries(obj).filter(
        (row) => row[1] !== undefined
    );

    const response = await fetch(`http://139.59.247.219:8000/cutting-stock-rices/${props['dataMain']["id"]}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body:JSON.stringify(Object.fromEntries(delPropsUndefined)),
    }).then((r) => r.json());

    if (response.error) {
      alert("กรุณาตรวจสอบข้อมูลให้ถูกต้อง !");
      setSpine(false);
    } else {
      alert("เพิ่มข้อมูลเสร็จสิ้น");
      window.document.getElementById("editRiceReset").click();
      setSpine(false);
    }
  };

  return (
    <div>
      <Head>
        <title>สหบูรณ์ข้าวไทย - แก้ไข จำหน่ายข้าวสาร</title>
      </Head>
      <div>
        <MainMenu callback={menuCallback}>
          <Spin spinning={spin} tip="กำลังโหลด...">
            <Row justify="center" style={{ marginTop: "1rem" }}>
              <Col span={24}>
                <h2>แก้ไข จำหน่ายข้าวสาร</h2>
              </Col>
            </Row>
            <Row justify="center" gutter={16} style={{ marginTop: "1rem" }}>
              <div className="card-form">
                <Form name="addPlaces" onFinish={onSave} layout={"horizontal"} initialValues={{
                    currentDate: moment(props['dataMain']['currentDate']),
                    millType: props['dataMain']['millType'],
                    weight: props['dataMain']['weight'],
                    description: props['dataMain']['description']
                }}>
                  <Form.Item
                    label="วันที่จำหน่าย"
                    name="currentDate"
                    rules={[{ required: true }]}
                  >
                    <DatePicker
                      style={{ width: '100%', maxWidth: 400 }}
                      locale={locale}
                      format="YYYY-MM-DD"
                      value={moment()}
                      placeholder="วันที่จำหน่าย"
                    />
                  </Form.Item>

                  <Form.Item
                    label="ประเภทข้าวสาร"
                    name="millType"
                    rules={[{ required: true }]}
                  >
                    <Select
                      showSearch
                      style={{ width: '100%', maxWidth: 400 }}
                      placeholder="ประเภทข้าวสาร"
                      optionFilterProp="children"
                      filterOption={(input, option) =>
                        option.children
                          .toLowerCase()
                          .indexOf(input.toLowerCase()) >= 0
                      }
                    >
                      <Option  value="start">ข้าวต้น</Option>
                      <Option  value="mid">ข้าวท่อน</Option>
                      <Option  value="final1">ข้าวปลาย 1</Option>
                      <Option  value="final2">ข้าวปลาย 2</Option>
                      <Option  value="machine">ข้าวหลังเครื่อง</Option>
                    </Select>
                  </Form.Item>

                  <Form.Item
                    label="น้ำหนัก / กก."
                    name="weight"
                    rules={[{ required: true }]}
                  >
                    {/* <InputNumber
                      placeholder="น้ำหนัก / กก."
                      style={{
                        width: '100%', maxWidth: 400,
                      }}
                      min={1}
                      max={5000}
                      step={1.0}
                    /> */}
                    <Input
                      type="number"
                      placeholder="น้ำหนัก / กก."
                      min="1"
                      max="5000"
                      style={{ width: "100%", maxWidth: 400 }}
                    />
                  </Form.Item>

                  <Form.Item
                    label="รายละเอียด"
                    name="description"
                    rules={[{ required: false }]}
                  >
                    <TextArea
                      placeholder="หมายเหตุ"
                      rows={3}
                      style={{ width: '100%', maxWidth: 400 }}
                    />
                  </Form.Item>

                  <Form.Item>

                    <Row gutter={8} justify="center">
                      <Col xs={0} sm={24}>
                        <center>
                          <Space>
                            <Button
                              style={{ margin: "0.5rem" }}
                              type="primary"
                              htmlType="submit"
                            >
                              บันทึก
                            </Button>

                            <Button
                              style={{ margin: "0.5rem" }}
                              id="editRiceReset"
                              type="danger"
                              htmlType="Reset"
                              onClick={() => {
                                router.push("/cuttingStockRice");
                              }}
                            >
                              ยกเลิก
                            </Button>
                          </Space>
                        </center>
                      </Col>

                      <Col xs={24} sm={0}>
                        <Button
                          style={{ margin: "0.2rem 0rem", width: "100%" }}
                          type="danger"
                          id="editRiceReset"
                          htmlType="Reset"
                          block
                          onClick={() => {
                            router.push("/cuttingStockRice");
                          }}
                        >
                          ยกเลิก
                        </Button>

                        <Button
                          style={{ margin: "0.2rem 0rem" }}
                          type="primary"
                          htmlType="submit"
                          block
                        >
                          บันทึก
                        </Button>
                      </Col>
                    </Row>
                    
                  </Form.Item>
                </Form>
              </div>
            </Row>
          </Spin>
        </MainMenu>
      </div>
    </div>
  );
}

export default cuttingStock;

export async function getServerSideProps(context) {
    const [dataMain, riceType] = await Promise.all([
      fetch(`http://139.59.247.219:8000/cutting-stock-rices/${context['params']['id']}`).then(r => r.json()),
      fetch(`http://139.59.247.219:8000/rice-type`).then(r => r.json())
    ]);
    
    return {
      props:{
         dataMain,
         riceType
      }
    }
}
