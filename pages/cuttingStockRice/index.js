import { useState } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import  { useEffect } from 'react';
import { Table, Space, Spin } from 'antd';
import { getUser } from '../../lib/user.js';
import MainMenu from "../../components/layout/main";
import { 
  Button,
  Form, 
  Row, 
  Col, 
  Divider,
  DatePicker,
  Tooltip,
  Typography } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import moment from 'moment';
import 'moment/locale/th';
import locale from 'antd/lib/locale/th_TH';
const { Title, Text } = Typography;

function CuttingStockRice(props){
  
  const router = useRouter();
  const [spin, setSpine] = useState(true);
  const [startValue,setStartValue] = useState(null);
  const [endValue,setEndValue] = useState(null);
  const [endOpen,setEndOpen] = useState(false);
  const [dateStart,setDateStart] = useState(moment().startOf('day'));
  const [dateEnd,setDateEnd] = useState(moment().endOf('day'));
  const [dataTable, setDataTable] = useState({data:[]});
  const millType = {
    start: "ข้าวต้น",
    mid: "ข้าวท่อน",
    final1: "ข้าวปลาย 1",
    final2: "ข้าวปลาย 2",
    machine: "ข้าวหลังเครื่อง"
  }

  const columns = [
    {
      title: 'ลำดับ',
      dataIndex: 'id',
      align: 'center',
      width:70,
      render:(text, record, index)=>{
        return (index + 1)
      }
    },
    {
      title: 'วันที่',
      dataIndex: 'currentDate',
      align: 'center',
      width: 150,
      render:(text, record, index)=>{
        return moment(text).format("YYYY-MM-DD HH:mm:ss")
      }
    },
    {
      title: 'ชนิดข้าว',
      dataIndex: 'millType',
      align: 'center',
      width: 100,
      render:(text, record, index)=>{
        return millType[text];
      }
    },
    {
      title: 'น้ำหนัก / กก.',
      dataIndex: 'weight',
      align: 'center',
      width: 100,
    },
    {
      title: 'จำหน่าย',
      dataIndex: 'cutType',
      align: 'center',
      width: 100,
    },
    {
      title: 'รายละเอียด',
      dataIndex: 'description',
      align: 'center',
      width: 200,
    },
    {
      title: 'จัดการ',
      dataIndex: 'id',
      align: 'center',
      width: 200,
      render:(text, record, index)=>{
          return (
            <Space>
              <Button style={{margin:'0.5rem'}} className="hover-warning" onClick={()=>{router.push(`/cuttingStockRice/${record['id']}`)}}>แก้ไข</Button>
              <Button style={{margin:'0.5rem'}} type="danger" onClick={()=>{handleDelete(record)}} >ลบ</Button>
            </Space>
          )
      }
    },
  ];

  useEffect(()=>{
    mainFunc();
  },[]);

  const mainFunc = () => {
    const user = getUser();
    if(user['profile'] === null) {
      router.push("/login");
    }else{
      init();
      setSpine(false);
    }
  }

  const init = async () => {
    const response = await fetch(`http://139.59.247.219:8000/cutting-stock-rices?filter=currentDate||$gte||${dateStart.format("YYYY-MM-DD HH:mm:ss")}&filter=currentDate||$lte||${dateEnd.format("YYYY-MM-DD HH:mm:ss")}&sort=currentDate,ASC`).then(r => r.json());
    if(!response.error){
      setDataTable(response);
    }
  }

  const menuCallback = (param) => {
    console.log("param -> ",param);
  }

  const handleDelete = async ({id}) => {

    let resultConfirm = confirm(`คุณกำลังจะลบ ข้อมูลนี้ ใช่หรือไม่ ?`);
    if(resultConfirm){

      setSpine(true);
      const response = await fetch(`http://139.59.247.219:8000/cutting-stock-rices/${id}`,{
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json'
        },
        body: null
      }).then(r => r);

      if(response.error){
          alert("เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง !");
          setSpine(false);
      }else{
          alert(`ลบข้อมูล เสร็จสิ้น`);
          router.push("/cuttingStockRice");
          init();
          setSpine(false);
      }

    }
  }

  const handleSearch = async () => {

    const response = await fetch(`http://139.59.247.219:8000/cutting-stock-rices?filter=currentDate||$gte||${dateStart.format("YYYY-MM-DD HH:mm:ss")}&filter=currentDate||$lte||${dateEnd.format("YYYY-MM-DD HH:mm:ss")}&sort=currentDate,ASC`).then(r => r.json());
    if(!response.error){
      setDataTable(response);
    }

  };

  const handleStartOpenChange = open => {
    if (!open) {
      setEndOpen(true);
    }
  };

  const handleEndOpenChange = open => {
    setEndOpen(open);
  };

  return (
    <div>
      <Head>
        <title>สหบูรณ์ข้าวไทย - จำหน่ายข้าวสาร</title>
      </Head>
      <div>
        <MainMenu callback={menuCallback} >
          <Spin  spinning={spin} tip="กำลังโหลด..." >
            <Row>
                <Col span={24}>
                  <div style={{marginTop:'1rem', marginBottom:'1rem'}}><Title level={4}>รายการ จำหน่ายข้าวสาร</Title></div>
                </Col>
            </Row>
            <Row>
                <Col xs={0} sm={0} md={16} lg={16}>
                  <Space>
                    วันที่เริ่มต้น: 
                    <DatePicker
                      style={{width:'100%', margin:'0.2rem 0'}}
                      locale={locale}
                      format="YYYY-MM-DD"
                      value={dateStart}
                      placeholder="วันที่เริ่มต้น"
                      onChange={(value)=>{setDateStart(value.startOf('day'))}}
                      onOpenChange={handleStartOpenChange}
                    />
                    วันที่สิ้นสุด: 
                    <DatePicker
                      style={{width:'100%', margin:'0.2rem 0'}}
                      locale={locale}
                      format="YYYY-MM-DD"
                      value={dateEnd}
                      placeholder="วันที่สิ้นสุด"
                      onChange={(value)=>{setDateEnd(value.endOf('day'))}}
                      onOpenChange={handleEndOpenChange}
                    />
                    <Tooltip title="ค้นหา">
                      <Button shape="circle" type="primary" onClick={()=>{handleSearch()}} icon={<SearchOutlined />} />
                    </Tooltip>
                  </Space>
                </Col>

                <Col xs={0} md={8}>
                  <a href="/cuttingStockRice/add">
                    <Button
                      style={{ float: "right" }}
                      type="primary"
                      shape="round"
                    >
                      จำหน่าย ข้าวสาร
                    </Button>
                  </a>
                </Col>

                <Col xs={24} sm={24} md={0} lg={0}>
                  <Row gutter={8}>
                    <Col xs={24} sm={24} md={0}>
                      <DatePicker
                        style={{width:'100%', margin:'0.2rem 0'}}
                        locale={locale}
                        format="YYYY-MM-DD"
                        value={dateStart}
                        placeholder="วันที่เริ่มต้น"
                        onChange={(value)=>{setDateStart(value.startOf('day'))}}
                        onOpenChange={handleStartOpenChange}
                      />
                    </Col>
                    <Col xs={24} sm={24} md={0}>
                      <DatePicker
                        style={{width:'100%', margin:'0.2rem 0'}}
                        locale={locale}
                        format="YYYY-MM-DD"
                        value={dateEnd}
                        placeholder="วันที่สิ้นสุด"
                        onChange={(value)=>{setDateEnd(value.endOf('day'))}}
                        onOpenChange={handleEndOpenChange}
                      />
                    </Col>
                    <Col xs={24} sm={24} md={0}>
                        <Button style={{margin:'0.2rem 0'}} block type="primary" onClick={()=>{handleSearch()}} icon={<span>ค้นหา <SearchOutlined /></span>} />
                    </Col>
                  </Row>
                </Col>

                <Col xs={24} md={0}>
                  <a href="/cuttingStockRice/add">
                    <Button
                      style={{ margin:'0.2rem 0' }}
                      type="primary"
                      shape="round"
                      block
                    >
                      จำหน่าย ข้าวสาร
                    </Button>
                  </a>
                </Col>

            </Row>
            <Row gutter={16}>
                <Col className="gutter-row" span={24}>
                    <Table 
                    style={{marginTop:'1rem', marginBottom:'1rem'}}
                    bordered
                    responsive
                    size="small"
                    columns={columns} 
                    dataSource={dataTable['data'].map((element, index)=>{return Object.assign(element,{key:index});})} 
                    pagination={{ pageSize: 25 }} scroll={{ y: 520 }} />
                </Col>
            </Row>

          </Spin>
        </MainMenu>
      </div>
    </div>
  )
}

export default CuttingStockRice;