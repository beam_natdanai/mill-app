import Head from 'next/head';
import axios from 'axios';
import { useRouter } from 'next/router';
import  { useEffect, useState } from 'react';
import { Table } from 'antd';
import { getUser } from '../../lib/user.js';
import MainMenu from "../../components/layout/main";
import { 
  Select, 
  Space, 
  Form,
  Button,
  InputNumber,
  Input,
  Spin,
  Row,
  Col
 } from 'antd';
const { Option } = Select;

function EmployeeUpdate(props){

    const router = useRouter();
    const [spin, setSpine] = useState(true);
    const [ricePrice, setRicePrice] = useState(0);
    const [carAndRiceWeight, setCarAndRiceWeight] = useState(0);
    const [carWeight, setCarWeight] = useState(0);

  useEffect(()=>{
    mainFunc();
  },[props])

  const mainFunc = () => {
    const user = getUser();
    if(user['profile'] === null) {
      router.push("/login");
    }else{
        setSpine(false)
    }
  }

  const menuCallback = (param) => {
    console.log("param -> ",param);
    router.push(`/${param}`);
  }

  const onSave = async ({updateEmployee}) => {

    setSpine(true);

    const obj = {
        firstName: updateEmployee.firstName,
        lastName: updateEmployee.lastName,
        username: updateEmployee.username,
        password: updateEmployee.password,
        telno: updateEmployee.telno
    }

    const delPropsUndefined = Object.entries(obj).filter((row)=>(row[1] !== undefined))
    const response = await fetch(`http://139.59.247.219:8000/user/${props['user']['id']}`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify( (Object.fromEntries(delPropsUndefined)) )
    }).then(r => r.json());

    if(response.error){
        alert("กรุณาตรวจสอบข้อมูลให้ถูกต้อง !");
        setSpine(false);
    }else{
        alert("แก้ไขข้อมูลเสร็จสิ้น");
        router.push("/employee");
        window.document.getElementById("updateEmployeeReset").click();
        setSpine(false);
    }

  }

  const onChangeRice = (val) => {
    setRicePrice(parseFloat(val));
  }

  const onChangeAllWeight = (val) => {
    setCarAndRiceWeight(parseFloat(val));
  }

  const onChangeCarWeight = (val) => {
    setCarWeight(parseFloat(val));
  }

  return (
    <div>
      <Head>
        <title>สหบูรณ์ข้าวไทย - แก้ไข พนักงาน</title>
      </Head>
      <div>
        <MainMenu callback={menuCallback} >
            <Spin  spinning={spin} tip="กำลังโหลด..." >
                <div className="row">
                    <h2>แก้ไข พนักงาน</h2>
                </div>
                <Row justify="center" style={{ marginTop: "1rem" }}>
                  <div className="card-form">
                        <Form name="updateEmployee" onFinish={onSave} layout={'horizontal'} initialValues={{ updateEmployee: {
                          firstName:props['user']['firstName'],
                          lastName:props['user']['lastName'],
                          username:props['user']['username'],
                          password:props['user']['password'],
                          telno:props['user']['telno']
                        } }}>

                          <Form.Item label="ชื่อจริง" name={['updateEmployee', 'firstName']} rules={[{ required: false }]} >
                              <Input placeholder="ชื่อจริง" style={{width: "100%"}} />
                          </Form.Item>

                          <Form.Item label="นามสกุล" name={['updateEmployee', 'lastName']} rules={[{ required: false }]} >
                              <Input placeholder="นามสกุล" style={{width: "100%"}} />
                          </Form.Item>

                          <Form.Item label="รหัสสมาชิก" name={['updateEmployee', 'username']} rules={[{ required: false }]} >
                              <Input placeholder="รหัสสมาชิก" style={{width: "100%"}} />
                          </Form.Item>

                          <Form.Item label="รหัสผ่าน" name={['updateEmployee', 'password']} rules={[{ required: false }]} >
                              <Input placeholder="รหัสผ่าน" style={{width: "100%"}} />
                          </Form.Item>

                          <Form.Item label="เบอร์ติดต่อ" name={['updateEmployee', 'telno']} rules={[{ required: false }]} >
                              <Input placeholder="เบอร์ติดต่อ" style={{width: "100%"}} />
                          </Form.Item>

                          <Form.Item >
                            <Row gutter={8} justify="center">
                              <Col xs={0} sm={24}>
                                <center>
                                  <Space>
                                    <Button style={{margin:'0.5rem'}} type="primary" htmlType="submit">
                                    บันทึก
                                    </Button>

                                    <Button style={{margin:'0.5rem'}} id="updateEmployeeReset" type="danger" htmlType="Reset" onClick={()=>{router.push("/employee");}} >
                                    ยกเลิก
                                    </Button>
                                  </Space>
                                </center>
                              </Col>
                              <Col xs={24} sm={0}>
                                <Button
                                  style={{ margin: "0.2rem 0rem", width: "100%" }}
                                  type="danger"
                                  id="updateEmployeeReset"
                                  htmlType="Reset"
                                  block
                                  onClick={() => {
                                    router.push("/employee");
                                  }}
                                >
                                  ยกเลิก
                                </Button>

                                <Button
                                  style={{ margin: "0.2rem 0rem" }}
                                  type="primary"
                                  htmlType="submit"
                                  block
                                >
                                  บันทึก
                                </Button>
                              </Col>
                            </Row>
                          </Form.Item>

                        </Form>
                    </div>
                </Row>
            </Spin>
        </MainMenu>
      </div>
    </div>
  )
}

export default EmployeeUpdate;

export async function getServerSideProps(context) {
  const [user] = await Promise.all([
    fetch(`http://139.59.247.219:8000/user/${context['params']['id']}`).then(r => r.json())
  ]);
  
  return {
    props:{
        user: user
    }
  }
}