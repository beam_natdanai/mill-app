import Head from "next/head";
import axios from "axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Table, DatePicker } from "antd";
import { getUser } from "../../lib/user.js";
import MainMenu from "../../components/layout/main";
import { Select, Space, Form, Button, InputNumber, Input, Spin, Row, Col } from "antd";
import moment from 'moment';
import 'moment/locale/th';
import locale from 'antd/lib/locale/th_TH';
import { useAppContext } from '../../context/state';
const { Option } = Select;

function rice(props) {
  const { user : { data } } = useAppContext();
  const router = useRouter();
  const [spin, setSpine] = useState(true);
  const [riceType, setRiceType] = useState(props['riceType']);

  useEffect(() => {
    mainFunc();
  }, []);

  const mainFunc = () => {
    const user = getUser();
    if (user["profile"] === null) {
      router.push("/login");
    } else {
      setSpine(false);
    }
  };

  const menuCallback = (param) => {
    console.log("param -> ", param);
    router.push(`/${param}`);
  };

  const onSave = async (_value) => {

    setSpine(true);

    const obj = {
      weight: parseFloat(_value.weight),
      activeStatus: "active",
      millType: _value.millType,
      riceType: parseFloat(_value.riceType),
      // package: _value.package,
      package:"general",
      user: data.id, 
      currentDate: moment(_value.currentDate).format("YYYY-MM-DD HH:mm:ss")
    }

    const response = await fetch(`http://139.59.247.219:8000/mill`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(obj),
    }).then((r) => r.json());

    if (response.error) {
      alert("กรุณาตรวจสอบข้อมูลให้ถูกต้อง !");
      setSpine(false);
    } else {
      alert("เพิ่มข้อมูลเสร็จสิ้น");
      window.document.getElementById("addRiceReset").click();
      setSpine(false);
    }

  };

  return (
    <div>
      <Head>
        <title>สหบูรณ์ข้าวไทย - เพิ่มข้าวสาร</title>
      </Head>
      <div>
        <MainMenu callback={menuCallback}>
          <Spin spinning={spin} tip="กำลังโหลด...">
            <Row justify="center" style={{ marginTop: "1rem" }}>
              <Col span={24}>
                <h2>เพิ่ม ข้าวสาร</h2>
              </Col>
            </Row>
            <Row justify="center" gutter={16} style={{ marginTop: "1rem" }}>
              <div className="card-form">
                <Form
                  name="addPlaces"
                  onFinish={onSave}
                  layout={"horizontal"}
                >
                  <Form.Item label="วันที่เพิ่มข้าว" name='currentDate' rules={[{ required: true }]} >
                    <DatePicker
                      style={{width: '100%', maxWidth: 400}}
                      locale={locale}
                      format="YYYY-MM-DD"
                      value={moment()}
                      placeholder="วันที่เพิ่มข้าว"
                    />
                  </Form.Item>

                  <Form.Item
                    label="ประเภทข้าว"
                    name="riceType"
                    rules={[{ required: true }]}
                  >
                    <Select
                        showSearch
                        style={{ width: '100%', maxWidth: 400 }}
                        placeholder="ประเภทข้าว"
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                          option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {riceType.data.map((row, index) => {
                            return <Option key={`riceType${index}`}  value={row.id}>{row.name}</Option>
                        })}
                      </Select>
                  </Form.Item>

                  <Form.Item
                    label="ประเภทข้าวสาร"
                    name="millType"
                    rules={[{ required: true }]}
                  >
                    <Select
                        showSearch
                        style={{ width: '100%', maxWidth: 400 }}
                        placeholder="ประเภทข้าวสาร"
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                          option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        <Option  value="start">ข้าวต้น</Option>
                        <Option  value="mid">ข้าวท่อน</Option>
                        <Option  value="final1">ข้าวปลาย 1</Option>
                        <Option  value="final2">ข้าวปลาย 2</Option>
                        <Option  value="machine">ข้าวหลังเครื่อง</Option>
                      </Select>
                  </Form.Item>

                  <Form.Item
                    label="น้ำหนัก / กก."
                    name="weight"
                    rules={[{ required: true }]}
                  >
                    <Input
                      type="number"
                      placeholder="น้ำหนัก / กก."
                      min="1"
                      max="5000"
                      style={{ width: "100%", maxWidth: 400 }}
                    />
                  </Form.Item>

                  {/* <Form.Item
                    label="บรรจุภัณฑ์"
                    name="package"
                    rules={[{ required: true }]}
                  >
                    <Select
                        showSearch
                        style={{ width: '100%', maxWidth: 400 }}
                        placeholder="ประเภท บรรจุภัณฑ์"
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                          option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        <Option  value="general">ทั่วไป</Option>
                        <Option  value="jumbo">จัมโบ้</Option>
                      </Select>
                  </Form.Item> */}

                  <Form.Item>
                    <Row gutter={8} justify="center">
                      <Col xs={0} sm={24}>
                        <center>
                          <Space>
                            <Button
                              style={{ margin: "0.5rem" }}
                              type="primary"
                              htmlType="submit"
                            >
                              บันทึก
                            </Button>

                            <Button
                              style={{ margin: "0.5rem" }}
                              id="addRiceReset"
                              type="danger"
                              htmlType="Reset"
                              onClick={() => {
                                router.push("/rice");
                              }}
                            >
                              ยกเลิก
                            </Button>
                          </Space>
                        </center>
                      </Col>

                      <Col xs={24} sm={0}>
                        <Button
                          style={{ margin: "0.2rem 0rem", width: "100%" }}
                          type="danger"
                          id="addRiceReset"
                          htmlType="Reset"
                          block
                          onClick={() => {
                            router.push("/rice");
                          }}
                        >
                          ยกเลิก
                        </Button>

                        <Button
                          style={{ margin: "0.2rem 0rem" }}
                          type="primary"
                          htmlType="submit"
                          block
                        >
                          บันทึก
                        </Button>
                      </Col>
                    </Row>
                  </Form.Item>
                </Form>
              </div>
            </Row>
          </Spin>
        </MainMenu>
      </div>
    </div>
  );
}

export default rice;

export async function getServerSideProps(context) {

  const [riceType] = await Promise.all([
    fetch(`http://139.59.247.219:8000/rice-type`).then(r => r.json())
  ]);
  
  return {
    props:{
       riceType: riceType
    }
  }
}