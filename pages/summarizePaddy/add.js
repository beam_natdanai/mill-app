import Head from "next/head";
import axios from "axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Table, DatePicker } from "antd";
import { getUser } from "../../lib/user.js";
import MainMenu from "../../components/layout/main";
import { useAppContext } from "../../context/state";
import {
  Select,
  Space,
  Form,
  Button,
  InputNumber,
  Input,
  Spin,
  Divider,
  Card,
  Row,
  Col
} from "antd";
import moment from "moment";
import "moment/locale/th";
import locale from "antd/lib/locale/th_TH";
const { Option } = Select;
const { TextArea } = Input;

function summarizePaddy(props) {
  const router = useRouter();
  const [spin, setSpine] = useState(true);
  const [accountStore, setAccountStore] = useState(null);
  let _useAppContext = useAppContext();
  
  useEffect(() => {
    mainFunc();
  }, []);

  const mainFunc = () => {
    const user = getUser();
    if (user["profile"] === null) {
      router.push("/login");
    } else {
      init();
      setSpine(false);
    }
  };

  const init = async () => {
    // console.log(`http://139.59.247.219:8000/account-store?limit=${props.riceType.count}&sort=riceType.id,DESC`);
    const response = await fetch(
      `http://139.59.247.219:8000/account-store?limit=${props.riceType.count}&sort=id,DESC`
    ).then((r) => r.json());
      // console.log(`response -> `,response);
    if (!response.statusCode) {
      setAccountStore(response);
    } else {
      setAccountStore({ data: [] });
    }
  };

  const menuCallback = (param) => {
    console.log("param -> ", param);
    router.push(`/${param}`);
  };

  const onSave = async (_value) => {
    const dateOld = (accountStore.count === 0 ? moment().format("YYYY-MM-DD") : moment(accountStore.data[0]['currentDate']).format("YYYY-MM-DD") ) ;
    const dateNew = _value.currentDate.format("YYYY-MM-DD");
    // console.log("Date ",moment(dateOld).isBefore(dateNew));
    if(accountStore.count === 0 || moment(dateOld).isBefore(dateNew) ){
      setSpine(true);
      
      const obj = props.riceType.data.map((row, index)=>{
        return {
          importationSumIn: 0,
          cuttingSumIn: 0,
          importationSumOut: parseFloat(_value[`importation_${row.id}`]),
          cuttingSumOut: parseFloat(_value[`cutting_${row.id}`]),
          balance: parseFloat(_value[`balance_${row.id}`]),
          description: parseFloat(_value['description']),
          currentDate: moment(_value.currentDate).format("YYYY-MM-DD HH:mm:ss"),
          day:  moment(_value.currentDate).format("DD"),
          month:  moment(_value.currentDate).format("MM"),
          year: moment(_value.currentDate).format("YYYY"),
          activeStatus: "active",
          riceType: row.id
        };
      });
      // console.log("obj -> ",obj);
      const response = await Promise.all(obj.map((row, index)=>{
        return fetch(`http://139.59.247.219:8000/account-store`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(row),
        }).then((r) => r.json())
      }));

      if (response.error) {
        alert("กรุณาตรวจสอบข้อมูลให้ถูกต้อง !");
        setSpine(false);
      } else {
        alert("เพิ่มข้อมูลเสร็จสิ้น");
        window.document.getElementById("addRiceReset").click();
        setSpine(false);
      }

    } else {
      alert(`วันที่ ${moment(_value.currentDate).format("DD/MM/YYYY")} มีข้อมูลอยู่แล้ว`);
    }

  };
 
  return (
    <div>
      <Head>
        <title>สหบูรณ์ข้าวไทย - สรุปยอด ข้าวเปลือก</title>
      </Head>
      <div>
        <MainMenu callback={menuCallback}>
          <Spin spinning={spin} tip="กำลังโหลด...">

            <Row justify="center" style={{ marginTop: "1rem" }}>
              <Col span={24}>
                <h2>สรุปยอด ข้าวเปลือก</h2>
              </Col>
            </Row>
    
            <Row justify="center" gutter={16} style={{ marginTop: "1rem" }}>
              
                <Col xs={24} sm={24} md={12} lg={12} >
                  {accountStore !== null &&
                    (accountStore.count === 0 ? (
                      
                      <Form
                        name="oldForm"
                        layout={"vertical"}
                        initialValues={Object.assign({currentDate: null},...props.riceType.data.map((row, index)=>{
                          return {
                            [`importation_${row.id}`]: 0,
                            [`cutting_${row.id}`]: 0,
                            [`balance_${row.id}`]: 0
                          }
                        }))}
                        // initialValues={Object.assign({currentDate: moment().add(-1, "days").startOf("day")},...props.riceType.data.map((row, index)=>{
                        //   return {
                        //     [`importation_${row.id}`]: 0,
                        //     [`cutting_${row.id}`]: 0,
                        //     [`balance_${row.id}`]: 0
                        //   }
                        // }))}
                      >
                        <Form.Item
                          label="ยอดยกมา วันที่"
                          name="currentDate"
                          rules={[{ required: false }]}
                        >
                          <DatePicker
                            disabled
                            style={{ maxWidth: 400, width: '100%' }}
                            locale={locale}
                            format="YYYY-MM-DD"
                            value={moment()}
                            placeholder="วันที่สรุปยอด"
                          />
                        </Form.Item>

                        {props.riceType.data.map((res, index) => {
                          return (
                            <Card
                              key={`riceType${index}`}
                              style={{ maxWidth: 400, width: '100%', margin: ".5rem .2rem" }}
                            >
                              <Divider
                                orientation="left"
                                plain
                                style={{ color: "#666666" }}
                              >
                                {`ยอดยกมา ${res.name}`}
                              </Divider>
                              <Form.Item
                                label="ยอดยกมา รับเข้า"
                                name={`importation_${res.id}`}
                                rules={[{ required: false }]}
                              >
                                {/* <InputNumber
                                  disabled
                                  placeholder="น้ำหนัก / กก."
                                  style={{
                                    width: 300,
                                  }}
                                  min={1}
                                  max={5000}
                                  step={1.0}
                                /> */}
                                <Input
                                  disabled
                                  type="number"
                                  placeholder="น้ำหนัก / กก."
                                  min="1"
                                  max="5000"
                                  style={{ width: "100%", maxWidth: 400 }}
                                />
                              </Form.Item>

                              <Form.Item
                                label="ยอดยกมา จำหน่ายออก"
                                name={`cutting_${res.id}`}
                                rules={[{ required: false }]}
                              >
                                {/* <InputNumber
                                  disabled
                                  placeholder="น้ำหนัก / กก."
                                  style={{
                                    width: 300,
                                  }}
                                  min={1}
                                  max={5000}
                                  step={1.0}
                                /> */}
                                <Input
                                  disabled
                                  type="number"
                                  placeholder="น้ำหนัก / กก."
                                  min="1"
                                  max="5000"
                                  style={{ width: "100%", maxWidth: 400 }}
                                />
                              </Form.Item>

                              <Form.Item
                                label="ยอดยกมา คงเหลือ"
                                name={`balance_${res.id}`}
                                rules={[{ required: false }]}
                              >
                                {/* <InputNumber
                                  disabled
                                  placeholder="น้ำหนัก / กก."
                                  style={{
                                    width: 300,
                                  }}
                                  min={1}
                                  max={5000}
                                  step={1.0}
                                /> */}
                                <Input
                                  disabled
                                  type="number"
                                  placeholder="น้ำหนัก / กก."
                                  min="1"
                                  max="5000"
                                  style={{ width: "100%", maxWidth: 400 }}
                                />
                              </Form.Item>
                            </Card>
                          );
                        })}

                        <Form.Item
                          label="รายละเอียด"
                          name="description"
                          rules={[{ required: false }]}
                        >
                          <TextArea
                            disabled
                            placeholder="หมายเหตุ"
                            rows={3}
                            style={{ width: '100%', maxWidth: 400 }}
                          />
                        </Form.Item>

                        {/* <Form.Item>
                          <Button
                            style={{ margin: "0.5rem" }}
                            type="primary"
                            htmlType="submit"
                          >
                            บันทึก
                          </Button>

                          <Button
                            style={{ margin: "0.5rem" }}
                            id="addRiceReset"
                            type="danger"
                            htmlType="Reset"
                            onClick={() => {
                              router.push("/summarizePaddy");
                            }}
                          >
                            ยกเลิก
                          </Button>
                        </Form.Item> */}
                      </Form>
                    
                    ) : (

                      <Form
                        name="oldForm"
                        layout={"vertical"}
                        initialValues={Object.assign({
                          currentDate: (accountStore !== null ? moment(accountStore.data[0]['currentDate']) : moment().add(-1, "days").startOf("day") )},
                          ...props.riceType.data.map((row, index)=>{
                          return {
                            [`importation_${row.id}`]: accountStore.data[index]['importationSumOut'],
                            [`cutting_${row.id}`]: accountStore.data[index]['cuttingSumOut'],
                            [`balance_${row.id}`]: accountStore.data[index]['balance']
                          }
                        }))}
                      >
                        <Form.Item
                          label="ยอดยกมา วันที่"
                          name="currentDate"
                          rules={[{ required: false }]}
                        >
                          <DatePicker
                            disabled
                            style={{ maxWidth: 400, width: '100%' }}
                            locale={locale}
                            format="YYYY-MM-DD"
                            value={(accountStore !== null && moment().format("YYYY-MM-DD HH:mm:ss"))}
                            placeholder="วันที่สรุปยอด"
                          />
                        </Form.Item>

                        {props.riceType.data.map((res, index) => {
                          return (
                            <Card
                              key={`riceType${index}`}
                              style={{ maxWidth: 400, margin: ".5rem .2rem" }}
                            >
                              <Divider
                                orientation="left"
                                plain
                                style={{ color: "#666666" }}
                              >
                                {`ยอดยกมา ${res.name}`}
                              </Divider>
                              <Form.Item
                                label="ยอดยกมา รับเข้า"
                                name={`importation_${res.id}`}
                                rules={[{ required: false }]}
                              >
                                {/* <InputNumber
                                  disabled
                                  placeholder="น้ำหนัก / กก."
                                  style={{
                                    width: '100%'
                                  }}
                                  min={1}
                                  max={5000}
                                  step={1.0}
                                /> */}
                                <Input
                                  disabled
                                  type="number"
                                  placeholder="น้ำหนัก / กก."
                                  min="1"
                                  max="5000"
                                  style={{ width: "100%", maxWidth: 400 }}
                                />
                              </Form.Item>

                              <Form.Item
                                label="ยอดยกมา จำหน่ายออก"
                                name={`cutting_${res.id}`}
                                rules={[{ required: false }]}
                              >
                                {/* <InputNumber
                                  disabled
                                  placeholder="น้ำหนัก / กก."
                                  style={{
                                    width: '100%'
                                  }}
                                  min={1}
                                  max={5000}
                                  step={1.0}
                                /> */}
                                <Input
                                  disabled
                                  type="number"
                                  placeholder="น้ำหนัก / กก."
                                  min="1"
                                  max="5000"
                                  style={{ width: "100%", maxWidth: 400 }}
                                />
                              </Form.Item>

                              <Form.Item
                                label="ยอดยกมา คงเหลือ"
                                name={`balance_${res.id}`}
                                rules={[{ required: false }]}
                              >
                                {/* <InputNumber
                                  disabled
                                  placeholder="น้ำหนัก / กก."
                                  style={{
                                    width: '100%'
                                  }}
                                  min={1}
                                  max={5000}
                                  step={1.0}
                                /> */}
                                <Input
                                  disabled
                                  type="number"
                                  placeholder="น้ำหนัก / กก."
                                  min="1"
                                  max="5000"
                                  style={{ width: "100%", maxWidth: 400 }}
                                />
                              </Form.Item>
                            </Card>
                          );
                        })}

                        <Form.Item
                          label="รายละเอียด"
                          name="description"
                          rules={[{ required: false }]}
                        >
                          <TextArea
                            disabled
                            placeholder="หมายเหตุ"
                            rows={3}
                            style={{ width: '100%', maxWidth: 400 }}
                          />
                        </Form.Item>
                      </Form>
                    ))}
                </Col>

                <Col xs={24} sm={12} md={12} lg={12} >
                  <Form name="addPlaces" onFinish={onSave} layout={"vertical"}>
                    <Form.Item
                      label="วันที่สรุปยอด"
                      name="currentDate"
                      rules={[{ required: true }]}
                    >
                      <DatePicker
                        style={{ maxWidth: 400, width: '100%' }}
                        locale={locale}
                        format="YYYY-MM-DD"
                        value={moment()}
                        placeholder="วันที่สรุปยอด"
                      />
                    </Form.Item>

                    {props.riceType.data.map((res, index) => {
                      return (
                        <Card
                          key={`riceType${index}`}
                          style={{ maxWidth: 400, width: '100%', margin: ".5rem .2rem" }}
                        >
                          <Divider
                            orientation="left"
                            plain
                            style={{ color: "#666666" }}
                          >
                            {res.name}
                          </Divider>
                          <Form.Item
                            label="รับเข้า"
                            name={`importation_${res.id}`}
                            rules={[{ required: true }]}
                          >
                            {/* <InputNumber
                              placeholder="น้ำหนัก / กก."
                              style={{
                                width: '100%'
                              }}
                              min={1}
                              max={5000}
                              step={1.0}
                            /> */}
                            <Input
                              type="number"
                              placeholder="น้ำหนัก / กก."
                              min="1"
                              max="5000"
                              style={{ width: "100%", maxWidth: 400 }}
                            />
                          </Form.Item>

                          <Form.Item
                            label="จำหน่ายออก"
                            name={`cutting_${res.id}`}
                            rules={[{ required: true }]}
                          >
                            {/* <InputNumber
                              placeholder="น้ำหนัก / กก."
                              style={{
                                width: '100%'
                              }}
                              min={1}
                              max={5000}
                              step={1.0}
                            /> */}
                            <Input
                              type="number"
                              placeholder="น้ำหนัก / กก."
                              min="1"
                              max="5000"
                              style={{ width: "100%", maxWidth: 400 }}
                            />
                          </Form.Item>

                          <Form.Item
                            label="คงเหลือ"
                            name={`balance_${res.id}`}
                            rules={[{ required: true }]}
                          >
                            {/* <InputNumber
                              placeholder="น้ำหนัก / กก."
                              style={{
                                width: '100%'
                              }}
                              min={1}
                              max={5000}
                              step={1.0}
                            /> */}
                            <Input
                              type="number"
                              placeholder="น้ำหนัก / กก."
                              min="1"
                              max="5000"
                              style={{ width: "100%", maxWidth: 400 }}
                            />
                          </Form.Item>
                        </Card>
                      );
                    })}

                    <Form.Item
                      label="รายละเอียด"
                      name="description"
                      rules={[{ required: false }]}
                    >
                      <TextArea
                        placeholder="หมายเหตุ"
                        rows={3}
                        style={{ width: '100%', maxWidth: 400 }}
                      />
                    </Form.Item>

                    <Form.Item>
                      <Row gutter={8} justify="center">
                        <Col xs={0} sm={24}>
                          <center>
                            <Space>
                              <Button
                                style={{ margin: "0.5rem" }}
                                type="primary"
                                htmlType="submit"
                              >
                                บันทึก
                              </Button>

                              <Button
                                style={{ margin: "0.5rem" }}
                                id="addRiceReset"
                                type="danger"
                                htmlType="Reset"
                                onClick={() => {
                                  router.push("/summarizePaddy");
                                }}
                              >
                                ยกเลิก
                              </Button>
                            </Space>
                          </center>
                        </Col>
                        <Col xs={24} sm={0}>
                          <Button
                            style={{ margin: "0.2rem 0rem", width: "100%" }}
                            type="danger"
                            id="addRiceReset"
                            htmlType="Reset"
                            block
                            onClick={() => {
                              router.push("/summarizePaddy");
                            }}
                          >
                            ยกเลิก
                          </Button>

                          <Button
                            style={{ margin: "0.2rem 0rem" }}
                            type="primary"
                            htmlType="submit"
                            block
                          >
                            บันทึก
                          </Button>
                        </Col>
                      </Row>
                    </Form.Item>
                  </Form>
                </Col>
              
            </Row>
          </Spin>
        </MainMenu>
      </div>
    </div>
  );
}

export default summarizePaddy;

export async function getServerSideProps(context) {
  const [riceType] = await Promise.all([
    fetch(`http://139.59.247.219:8000/rice-type`).then((r) => r.json()),
  ]);

  return {
    props: {
      riceType: riceType,
    },
  };
}
