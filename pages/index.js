import Head from 'next/head';
import { useRouter } from 'next/router';
import  { useEffect } from 'react';
import { getUser } from '../lib/user.js';
import MainMenu from "../components/layout/main";
import { signOut } from '../lib/user';
import { useAppContext } from '../context/state';

const Home = (props) => {

  const router = useRouter();
  let _useAppContext = useAppContext();

  useEffect(()=>{
    mainFunc();
  },[props])

  const mainFunc = () => {
    const user = getUser();
    if(user['profile'] === null) {
      router.push("/login");
    } else {
      _useAppContext.user.update({
        role: user['profile']['username']['role']['roleName'],
        firstName: user['profile']['username']['firstName'],
        lastName: user['profile']['username']['lastName']
      });
      router.push("/buyRice");
    }
  }

  const menuCallback = (param) => {
    router.push(`${param}`);
  }

  return (
    <div>
      {/* <Head>
        <title>สหบูรณ์ข้าวไทย - เมนูหลัก</title>
      </Head>
      <div style={{padding:'1rem'}}>
          <button style={{borderRadius: 8, height: 36, minWidth: 100, cursor:'pointer', borderStyle: 'solid', borderWidth: 3, borderColor: '#e83c3c'}} onClick={()=>{signOut(); router.push("/login");}}>ออกจากระบบ</button> &nbsp;&nbsp;<span>{`User : ${_useAppContext['user']['data']['firstName']} ${_useAppContext['user']['data']['lastName']}`}</span>
      </div>
      <div id="mainmenu-desktop" className="vh98 vertical-center section">
        <div  className="container" style={{width:800}} >
          <div className="row">
            <div className="col-12 section">
                <div className="menu-titile shadow-black"><span className="menu-text-title">ระบบงาน</span></div>
            </div>
          </div>
          <div className="row" style={{marginTop:'2rem'}}>
          {( (_useAppContext['user']['data']['role'] !== 'employee' && _useAppContext['user']['data']['role'] !== null ) && 
            <div className="col-6">
                <center><a href="/report/importedRice"><div className="menu-box vertical-center"><span className="menu-text-issue shadow-black">ผู้บริหาร</span></div></a></center>
            </div>
          )}
            <div className="col-6" style={{textAlign:'center'}}>
              <center><a href="/buyRice"><div className="menu-box vertical-center"><span className="menu-text-issue shadow-black">รับเข้าข้าว</span></div></a></center>
            </div>
            <div className="col-6">
                <center><a href="/cuttingStock"><div className="menu-box vertical-center"><span className="menu-text-issue shadow-black">จัดการคลัง</span></div></a></center>
            </div>
            <div className="col-6" style={{textAlign:'center'}}>
              <center><a href=""><div className="menu-box vertical-center"><span className="menu-text-issue shadow-black">ขาย</span></div></a></center>
            </div>
          </div>
        </div>
      </div>
        
      <div id="mainmenu-mobile" className="container" style={{width:300}} >
        <div className="row" style={{marginTop:'1.2rem'}}>

            <div className="col-12">
                <center><div className="menu-titile shadow-black"><span className="menu-text-title">ระบบงาน</span></div></center>
                <br/>
            </div>
          
          {( (_useAppContext['user']['data']['role'] !== 'employee' && _useAppContext['user']['data']['role'] !== null ) && 
            <div className="col-12">
                <center><a href="/report/importedRice"><div className="menu-box vertical-center"><span className="menu-text-issue shadow-black">ผู้บริหาร</span></div></a></center>
            </div>
          )}
          <div className="col-12" style={{textAlign:'center'}}>
            <center><a href="/buyRice"><div className="menu-box vertical-center"><span className="menu-text-issue shadow-black">รับเข้าข้าว</span></div></a></center>
          </div>
          <div className="col-12">
              <center><a href="/cuttingStock"><div className="menu-box vertical-center"><span className="menu-text-issue shadow-black">จัดการคลังข้าว</span></div></a></center>
          </div>
          <div className="col-12" style={{textAlign:'center'}}>
            <center><a href=""><div className="menu-box vertical-center"><span className="menu-text-issue shadow-black">ขาย</span></div></a></center>
          </div>
        </div>
      </div> */}

      
    </div>
  )
}

export default Home;