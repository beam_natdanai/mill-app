export const arrSum = arr => arr.reduce((a,b) => a + b, 0)

export const arrSumKey = (arr,_key) => arr.reduce((a,b) => a + (b[_key] || 0), 0)

export const sumForGroup = (obj) => {

    return Object.entries(obj).map((row)=>{
        return {
            name: row[0],
            ...row[1].reduce((past, current, index) => {   
                return {
                    createdAt: current.createdAt,
                    ricePriceTotal: (past['ricePriceTotal'] + current.ricePrice),
                    ricePriceDivide: (index+1),
                    netRicePriceTotal: (past['netRicePriceTotal'] + current.netRicePrice),
                    riceWeightTotal: (past['riceWeightTotal'] + current.riceWeight)
                };
            }, {
                createdAt: null,
                ricePriceTotal: 0,
                ricePriceDivide: 0,
                netRicePriceTotal: 0,
                riceWeightTotal: 0
            }) 
        };
    });
    
}

export const groupByAndValue = (key) => array =>
  array.reduce(
    (objectsByKeyValue, obj) => ({
      ...objectsByKeyValue,
    [obj[key]]: (objectsByKeyValue[obj[key]] || []).concat(obj)
    }),
    {}
); 