
export const signIn = ( _profile, _role) => {
    localStorage.setItem("profile", JSON.stringify({ username: _profile }));
    localStorage.setItem("role", JSON.stringify({role_name:_role}));
    return {status: true}
}

export const signUp = () => {
        
}

export const signOut = () => {
    localStorage.removeItem('profile');
    localStorage.removeItem('role');
}

export const getUser = () => {
    const profile = JSON.parse(localStorage.getItem('profile'));
    return {profile: profile};
}

